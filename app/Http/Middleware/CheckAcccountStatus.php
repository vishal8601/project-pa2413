<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Enums\AccountStatus;
use Illuminate\Support\Facades\Auth;

class CheckAcccountStatus
{
    public function handle(Request $request, Closure $next): Response
    {
        // Retrieve the authenticated person
        $person = auth()->user();

        // return response()->json(array('person' => $person));

        // Ensure the person is authenticated and check their status
        if ($person && $person->status === AccountStatus::Suspend) {
            return response('🚫 Error: 403 Forbidden, You cannot access information as it is blocked by the admin');
        }

        return $next($request);
    }
}
