<?php

namespace App\Http\Controllers;

use App\Models\BuyerEdit;
use App\Models\SellerEdit;
use App\Models\PicBuyer;
use App\Models\PicSeller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\Mailing;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function blogposts()
    {
        return view('blogposts');
    }

    public function login()
    {
        return view('login');
    }
    public function aboutBuyer()
    {
        // retrieving various DB tables by the models to the view of that page
        $buyerEdit = BuyerEdit::all();
        $pictures = PicBuyer::all();
        return view('aboutBuyer', compact('buyerEdit', 'pictures'));
    }

    public function aboutSeller()
    {
        $sellerEdit = SellerEdit::all();
        $pictures = PicSeller::all();
        return view('aboutSeller', compact('sellerEdit', 'pictures'));
    }
}