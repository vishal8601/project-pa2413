<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Person;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        // Check if the user exists and is verified
        $user = Person::where('email', $credentials['email'])->first();

        if (!$user) {
            session()->flash('error', 'Invalid credentials');
            return redirect()->back()->withInput()->withErrors(['email' => 'Invalid credentials']);
        }
        if (!$user->email_verified_at) {
            session()->flash('error', 'Email not verified, please verify your email and try again.');
            return redirect()->back()->withInput()->withErrors(['email' => 'Email not verified']);
        }
        if (Auth::attempt($credentials)) {
            $request->session()->put('user_type', Auth::user()->type);
            return redirect()->intended('home');
        }
        session()->flash('error', 'Invalid credentials');
        return redirect()->back()->withInput()->withErrors(['email' => 'Invalid credentials']);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        session()->flash('status', 'logged out');
        return redirect()->route('home');
    }
}
