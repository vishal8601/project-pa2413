<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use App\Models\Person;

class PremiumController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function showSubscribeForm($personId)
    {
        $person = Person::findOrFail($personId);
        return view('subscription.subscribe_form', compact('person'));
    }

    public function createCheckoutSession(Request $request)
    {
        $request->validate([
            'person_id' => 'required|integer|exists:persons,id',
        ]);

        $person = Person::find($request->person_id);

        try {
            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => [
                    [
                        'price' => 'your_stripe_price_id', // We need to replace this with the stripe product ID.
                        'quantity' => 1,
                    ]
                ],
                'mode' => 'subscription',
                'success_url' => route('checkout.success', ['person_id' => $person->id]),
                'cancel_url' => route('checkout.cancel'),
            ]);

            return response()->json(['id' => $session->id]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function success(Request $request)
    {
        $person = Person::find($request->person_id);
        $person->premium_user = true;
        $person->save();

        return view('subscription.subscribed_success', compact('person'));
    }

    public function cancel()
    {
        return view('subscription.subscribed_cancelled');
    }
}
