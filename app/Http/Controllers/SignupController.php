<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\VerificationMail;

class SignupController extends Controller
{
    public function store(Request $request)
    {
        // Validate user input
        $validatedData = $request->validate([
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|email|unique:persons,email',
            'type' => 'required|in:buyer,seller',
            'newPassword' => 'required|string',
        ]);
        // Hash the password
        // for security means
        $hashedPassword = Hash::make($validatedData['newPassword']);
        $verificationToken = Str::random(64);  // Generate a verification token

        // Create new user
        $user = Person::create([
            'first_name' => $validatedData['firstName'],
            'last_name' => $validatedData['lastName'],
            'email' => $validatedData['email'],
            'type' => $validatedData['type'],
            'password' => $hashedPassword,
            'verification_token' => $verificationToken, // Save the verification token
        ]);
        // Send verification email
        $subject = 'Welcome to the horticulture website';
        $body = 'To verify your sign in click the link below!';
        $verificationLink = url('/verify-email?token=' . $verificationToken);

        Mail::to($user->email)->send(new VerificationMail($subject, $body, $verificationLink));

        return redirect()->route('login')->with('status', 'User created successfully. Please check your email for verification.');
    }
    public function index()
    {
        $persons = Person::all(); // Fetch all persons
        return view('admin.adminaccounts', compact('persons')); // Pass the persons data to the view
    }
}
