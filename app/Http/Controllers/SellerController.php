<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\Bid;
use App\Models\Person;
use Illuminate\Support\Facades\Auth;


class SellerController extends Controller
{
    public function myaccount()
    {
        return view('seller.myaccount');
    }
    public function seller()
    {

        return view('seller.seller');
    }

    public function sellerbrowse(Request $request)
    {
        // Check if a search query is provided
        $search = $request->input('search');
    
        if (!empty($search)) {
            // Fetch project that match the query by the seller
            $projects = Project::where('title', 'like', '%' . $search . '%')
            // below is to show project that dont have an accepted bid
                ->whereDoesntHave('bids', function ($query) {
                    $query->where('accepted', true);         
                })
                ->with('buyer')
                ->get();
        } else {
            // Fetch all projects with buyer information and filter out projects with accepted or rejected bids if no search term is provided
            $projects = Project::whereDoesntHave('bids', function ($query) {
                    $query->where('accepted', true);        
                })
                ->with('buyer')
                ->get();
        }
    
        return view('seller.sellerbrowse', compact('projects'));
    }
    
// full display of each project when seller click on one from the sellerbrowse view
    public function each_project($id)
    {
        $project = Project::findOrFail($id);
        $bids = Bid::where('project_id', $id)->get();
        return view('seller.each_project', compact('project','bids'));
    }

    //store that bid that will be linked to the project
    public function bid_store(Request $request, $projectId, $sellerId)
{
    // Validate the request data
    $validatedData = $request->validate([
        'amount' => 'required|numeric',
        'comment' => 'required|string', 
    ]);
    
   //checking if user already made a bid
    $existingBid = Bid::where('project_id', $projectId)
                       ->where('seller_id', $sellerId)
                       ->first();

    if ($existingBid) {
        // Redirect back with an error message if a bid already exists
        return redirect()->back()->with('Only once mate');
    }
    // Create the bid
    $bid = new Bid();
    $bid->project_id = $projectId;
    $bid->seller_id = $sellerId;
    $bid->amount = $validatedData['amount'];
    $bid->comment = $validatedData['comment']; 
    $bid->save();
    return redirect()->back();
}


}


