<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;

class EmailController extends Controller
{
    public function verifyEmail(Request $request)
    {
        $token = $request->query('token');

        // Find the person with the matching verification token
        $person = Person::where('verification_token', $token)->first();

        if ($person) {
            // Mark the person as verified and clear the token
            $person->email_verified_at = now();
            $person->verification_token = null;
            $person->save();
            session()->flash('status', 'Email verified successfully, you can login now');
        } else {
            session()->flash('error', 'Invalid verification token');
        }

        return redirect('/login'); // Redirect to the home page or another page
    }
}



