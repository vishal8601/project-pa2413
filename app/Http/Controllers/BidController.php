<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bid;
use App\Models\BidNotification;
use App\Models\Submitt;
use App\Models\Project;
use App\Models\Invitation;
use Illuminate\Support\Facades\Auth;

class BidController extends Controller
{
    // defines an accepted bid, so that seller knows
    public function acceptBid($bidId)
{
    $bid = Bid::findOrFail($bidId);
    $bid->accepted = true;
    $bid->save();

    // Creating a Bid notification for the accepted bid
    BidNotification::create([
        'seller_id' => $bid->seller_id,
        'buyer_id' => $bid->project->buyer_id,
        'project_id' => $bid->project_id,
        'project_name' => $bid->project->title,
        'notification_message' => 'Your bid was accepted'
    ]);
    // other bids is  means the rejected bids
    $otherBids = Bid::where('project_id', $bid->project_id) ->where('id', '!=', $bidId) ->get();

    foreach ($otherBids as $otherBid) {  
        $otherBid->delete(); // when deleted, marked as rejected,
        // after the deletion, create a notification for the rejected bid
        BidNotification::create([
            'seller_id' => $otherBid->seller_id,
            'buyer_id' => $bid->project->buyer_id,
            'project_id' => $bid->project_id,
            'project_name' => $bid->project->title,
            'notification_message' => 'Your bid was rejected'
        ]);
    }
    return redirect()->back();
}
public function destroyNotify($id)
{
    $notification = BidNotification::findOrFail($id);
    $notification->delete();
    return redirect()->back();
}

public function notify()
{
    if (Auth::check() && Auth::user()->type === 'seller') {
        $sellerId = Auth::id();

    // Retrieve notifications for the logged-in seller with project and buyer details
$notifications = BidNotification::where('seller_id', $sellerId)
        ->with(['project' => function($query) {
             $query->with('buyer');
                    }])
                    ->get();

        // Retrieve all accepted bids associated with this user with project and buyer details
        $bids = Bid::with(['project.buyer', 'seller'])
                   ->where('seller_id', $sellerId)
                   ->where('accepted', true)
                   ->get();
        // Retrieve all invitations for the logged-in seller
        $invitations = Invitation::where('seller_id', $sellerId)
                                 ->with('project', 'buyer')
                                 ->get();
        // Pass notifications, bids, and invitations to the view
        return view('seller.notify', [
            'notifications' => $notifications,
            'bids' => $bids,
            'invitations' => $invitations,
            'success' => session('success')  // Pass any session success message to the view
        ]);
    } 
}
    public function buyerNotify()
    {
        // Retrieve all submitts for the logged-in user
        $submitts = Submitt::where('buyer_id', Auth::id())->get();
        return view('buyer.buyerNotify', ['submitts' => $submitts]);
    }
    // this is where seller input form, based or queryed information and infomration of payement details
    public function storeSubmitt(Request $request)
    {
        $validated = $request->validate([
        'project_title' => 'required|max:255',
        'project_description' => 'required',
        'timelines' => 'required', 
        'buyer_full_name' => 'required|max:255',
        'seller_full_name' => 'required|max:255',
        'bid_amount' => 'required|numeric',
        'bid_comment' => 'required',
        'seller_email' => 'required|email',
        'buyer_id' => 'required|numeric',
        'phone' => 'required', 
        'additional_comment'=> 'nullable', // lets make this nullable
        ]);
        Submitt::create($validated);

        
        $project = Project::find($request->input('project_id'));

    if ($project) { $project->delete(); }

    return redirect()->back()->with('success', 'Submission created successfully and associated project deleted.');
   



    }
    public function destroySubmitt($id)
{
    $submitt = Submitt::findOrFail($id);
    $submitt->delete();
    return redirect()->back();
}

    // Other methods can be added here
}
