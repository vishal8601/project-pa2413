<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function store(Request $request)
    {
        // to validate everything
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'timelines' => 'nullable|string',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048', 
        ]);
        // link the project with the logged in buyer
        $validatedData['buyer_id'] = auth()->id();
       //using array to store the photos
        $photos = [];
        if ($request->hasFile('photos')) {
         foreach ($request->file('photos') as $photo) 
           {
             $path = $photo->store('public/photos'); // Store the photo in the 'photos' directory
             $photos[] = ['path' => $path]; // Add the photo path to the array
           }
        }
        $photosJson = json_encode($photos);
        $validatedData['photos'] = $photosJson;
        Project::create($validatedData);
        return redirect()->back();
    }
}

