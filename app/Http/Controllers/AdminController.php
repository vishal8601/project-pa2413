<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;
use App\Enums\AccountStatus;

class AdminController extends Controller
{
  
    // AMIN FUNCTIONALITY
    public function adminaccounts()
    {
       //navigate to the page where it shows all the users that use the website
        $persons = Person::all(); 
        return view('admin.adminaccounts', compact('persons')); //go to admin.adminaccounts view, holds the persons table
        // have to use $persons in the correct view to show the data, like retreiving the table
    }
    public function deletePerson($id)
    {
        $person = Person::findOrFail($id); // fids the id
        $person->delete(); // to delete them
        return redirect()->back(); //ensure that they stay on same page
    }


    // Suspend is where user cannot login
    // Unsuspend is when user can login
    public function suspendPerson($id)
    {
        $person = Person::findOrFail($id);
        $person->status = AccountStatus::Suspend; //  Sets the person's status to Suspend
        $person->save();
        return redirect()->back();
    }
    public function unsuspendPerson($id)
    {
        $person = Person::findOrFail($id);
        $person->status = AccountStatus::Active; //Sets the person's status to Unsuspend
        $person->save();
        return redirect()->back();
    }

    public function showPersonDetails($id)
    {
        // the Person model, where it links all the associated tables
        $person = $person = Person::with(['projects', 'services', 'credentials', 'experiences', 'ProfilePics', 'projects'])->findOrFail($id);
        return view('admin.userAccount', compact('person'));
    }
    public function adminhome()
    {
        return view('admin.adminhome');
    }
    public function adminmyaccount()
    {
        return view('admin.adminmyaccount');
    }
    public function adminposts()
    {
        return view('admin.adminposts');
    }
}




