<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Blog;
use App\Models\Person;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::with('author')->get();
        return view('blogposts', compact('blogs'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Validate image file
        ]);

        // Handle image upload
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/images', $imageName);
        } else {
            $imageName = null;
        }
        Blog::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'image' => $imageName,
            'user_id' => Auth::id(), // Assign the current user's ID as the author
        ]);
        return redirect()->route('blogposts')->with('status', 'Blog created successfully.');
    }
    public function update(Request $request, Blog $blog)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Validate image file
        ]);

        // Handle image upload
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/images', $imageName);
            // Delete previous image if exists
            if ($blog->image) {
                Storage::delete('public/images/' . $blog->image);
            }
        } else {
            $imageName = $blog->image;
        }
        $blog->update([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'image' => $imageName,
        ]);
        return redirect()->route('blogposts')->with('status', 'Blog updated successfully.');
    }
    public function destroy(Blog $blog)
    {
        $blog->delete();
        return redirect()->route('blogposts')->with('status', 'Blog deleted successfully.');
    }

}