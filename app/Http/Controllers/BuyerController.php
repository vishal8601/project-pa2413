<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use App\Models\Project;
use App\Models\Bid;
use App\Models\Service;
use App\Models\Invitation;
use Illuminate\Support\Facades\Auth;

class BuyerController extends Controller
{
    public function buyer()
    {
        return view('buyer.buyer');
    }

    public function myaccountbuyer()
    {
        return view('buyer.myaccountbuyer');
    }

    public function myprojects()
    {
        $projects = auth()->user()->projects;
        $services = Service::all(); // filter the services as needed
        return view('buyer.myprojects', compact('projects', 'services'));
    }


    public function each_project($id)
    {
        $project = Project::findOrFail($id);
        $bids = Bid::where('project_id', $id)->get();
        return view('buyer.individual_project', ['project' => $project, 'bids' => $bids]);

    }
    /* this if for searching seller by service listing.
      see are service a buyer like, then view the sellers profile
    */
    public function searching(Request $request)
    {
        $query = $request->input('query');
        $services = Service::where('name', 'LIKE', '%' . $query . '%')
            ->with('user') // eager load the related user
            ->paginate(10); // search query, basically result similar words to the query
        return view('buyer.searchbuyer', compact('services'));
    }
    //when click on a service, goes page that shows the sellers details who provides that service
    public function show($id)
    {
        $person = Person::with(['projects', 'services', 'credentials', 'experiences', 'ProfilePics'])->findOrFail($id);
        return view('buyer.theSeller', compact('person'));
    }
// I am not sure if this is needed???
    public function update(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'type' => 'required|in:buyer,seller',
        ]);
        $userId = auth()->id();
        Person::where('id', $userId)->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'type' => $request->input('type'),
        ]);
        return redirect()->route('buyer.myaccount')->with('success', 'Profile updated successfully');
    }

    // Buyer has the ability to delete project
    public function delete_project($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();
        // Provide a  flash message for success
        session()->flash('status', 'Project deleted successfully');

        return redirect()->route('buyer.myprojects');
    }

    // Invitation request
    public function send(Request $request)
    {
        // to ensure that these exist in the server/database
        $request->validate([
            'project_id' => 'required|exists:projects,id',
            'seller_email' => 'required|email|exists:persons,email',
        ]);
        //to find the peson with the email, otherwise, give an error
        $seller = Person::where('email', $request->seller_email)->firstOrFail();
        $buyer_id = Auth::id(); // get the person id that is logged in, who should be a buyer

        Invitation::create([ //create a new invitation
            'project_id' => $request->project_id,
            'seller_id' => $seller->id,
            'buyer_id' => $buyer_id,
        ]);
        session()->flash('status', 'Invitation sent successfully.');
        return redirect()->back();
    }
}