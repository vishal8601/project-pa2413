<?php
namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Credential;
use App\Models\Experience;
use App\Models\ProfilePic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    // SAME FUNCTION, BUT FOR DIFFERENT TABLES
   
    public function myAccount()
{
    // Retrieve services, credentials, and experiences associated with the currently logged-in user
    $services = Auth::user()->services;
    $credentials = Auth::user()->credentials;
    $experiences = Auth::user()->experiences; // Load experiences as well
    $pictures = ProfilePic::where('person_id', Auth::id())->get(); // Load profile pictures
    // Pass services, credentials, experiences, and pictures to the view
     return view('seller.myaccount', compact('services', 'credentials', 'experiences', 'pictures'));
}

  //SERVICES LISTING

    public function store(Request $request) //request is incomming data send to the server/database by user
    {
        $request->validate([
            'name' => 'required|string|max:255','description' => 'required|string'
        ]);
        $service = new Service();

        // seller_id is the foreign key to logged in person
        $service->seller_id = Auth::id(); // Link the service to the logged-in user
        // below is saving what the user has requested to be saved
        $service->name = $request->name;
        $service->description = $request->description;
        $service->save();

        return redirect()->back();
    }
    public function updateService(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255', 'description' => 'required|string'
        ]);
        $service = Service::find($id);
        $service->name = $request->name;
        $service->description = $request->description;
        $service->save();
        
        return redirect()->back();
    }

    public function deleteService($id)
{
    $service = Service::find($id);
    $service->delete();
    return redirect()->back();
}
// CREDIENTIALS
    public function CredStore(Request $request)
    {
        $request->validate([
            'credential_name' => 'required|string|max:255','institution_name' => 'required|string|max:255'
        ]);

        $credential = new Credential();
        $credential->seller_id = Auth::id(); // Link the credential to the logged-in user
        $credential->credential_name = $request->credential_name;
        $credential->institution_name = $request->institution_name;
        $credential->save();
        return redirect()->back();
    }


    public function updateCredential(Request $request, $id)
{
    $request->validate([
        'credential_name' => 'required|string|max:255', 'institution_name' => 'required|string'
    ]);
    $credential = Credential::find($id);
    $credential->credential_name = $request->credential_name;
    $credential->institution_name = $request->institution_name;
    $credential->save();
    return redirect()->back();
}

public function deleteCredential($id)
{
    $credential = Credential::find($id);
    $credential->delete();
    return redirect()->back();
}

//EXPERIENCES
public function storeExperience(Request $request)
{
    $request->validate([
        'job_title' => 'required|string|max:255','company_name' => 'nullable|string|max:255',
        'description' => 'nullable|string'
    ]);
    $experience = new Experience();
    $experience->seller_id = Auth::id(); 
    $experience->job_title = $request->job_title;
    $experience->company_name = $request->company_name;
    $experience->description = $request->description;
    $experience->save();
    return redirect()->back();
}

public function updateExperience(Request $request, $id)
{
    $request->validate([
        'job_title' => 'required|string|max:255','company_name' => 'nullable|string|max:255',
        'description' => 'nullable|string'
    ]);
    $experience = Experience::find($id);
    $experience->job_title = $request->job_title;
    $experience->company_name = $request->company_name;
    $experience->description = $request->description;
    $experience->save();
    return redirect()->back();
}


public function deleteExperience($id)
{
    $experience = Experience::find($id);
    $experience->delete();
    return redirect()->back();
}
// link photo to the person, only sellers can do this,
// same as the EditablesText Controller for saving images but is linked to the person
public function profilePic(Request $request)
    {
        $person = Auth::user();
        $existingPicturesCount = ProfilePic::where('person_id', $person->id)->count();
        //want to limit to just three people
        if ($existingPicturesCount >= 3) {
            return redirect()->route('profile.photos.index')->with('error', 'You can only upload up to 3 pictures.');
        }

     /*   $validatedData = $request->validate([
            'pictures.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
*/
        $pictures = [];
        if ($request->hasFile('pictures')) {
            foreach ($request->file('pictures') as $picture) {
                if ($existingPicturesCount >= 3) {
                    break;
                }
                $path = $picture->store('public/profile_pics');
                $pictures[] = ProfilePic::create(['person_id' => $person->id, 'filename' => $path]);
                $existingPicturesCount++;
            }
        }

        return redirect()->route('seller.myaccount')->with('success', 'Pictures uploaded successfully');
    }

    public function profileDestroy($id)
    {
        $picture = ProfilePic::findOrFail($id);

        if ($picture->person_id != Auth::id()) {
            return redirect()->route('seller.myaccount')->with('error', 'Unauthorized action.');
        }

        Storage::delete($picture->filename);
        $picture->delete();

        return redirect()->route('seller.myaccount')->with('success', 'Picture deleted successfully');
    }


    



    




















}








