<?php

namespace App\Http\Controllers;

use App\Models\EditableText;
use App\Models\BuyerEdit;
use App\Models\SellerEdit;
use App\Models\Picture;
use App\Models\PicBuyer;
use App\Models\PicSeller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/* THIS IS FOR THE LANDING PAGES
   WHERE ADMIN CAN MODITY THE PAGES AS NEEDED
*/

class EditableTextController extends Controller
{
    public function index() // This is to show the text and pictures for the home page
    {
      //EACH ONE IS RETRIEVING THE INFORMATION FROM  A DATABASE TABLE
    $editableTexts = EditableText::all();
    $pictures = Picture::all();
    return view('home', compact('editableTexts', 'pictures'));
    }


    // abiliy to update
    public function update(Request $request) {
    /* request is expecting incoming data that is given
     or requested by the user */
      
        //find the associated ID to update the sepecific row within databse
        $editableText = EditableText::findOrFail($request->id); 
        $editableText->update([
            'title' => $request->title,
            'content' => $request->content
        ]);

        return response()->json(['success' => true]); // to reload the page that was within the json code in the view page
    }
    
  
    // delete the associuate row
    public function delete(Request $request)
{
    $editableText = EditableText::findOrFail($request->id);
    $editableText->delete();
    return response()->json(['success' => true]);
}

// to add a new text to the home page
// in DB table, is another record
public function store(Request $request)
{
    $newRecord = new EditableText;
    $newRecord->title = 'New Title';
    $newRecord->content = 'New Content';
    $newRecord->save();
    return response()->json(['success' => true]);
}


// BUYER ABOUT EDIT PAGE FOR ADMIN
    public function buyerUpdate(Request $request)
    {
        $buyerEdit = BuyerEdit::findOrFail($request->id);
        $buyerEdit->update([
            'title' => $request->title,
            'content' => $request->content
        ]);
        return response()->json(['success' => true]);

    }

    public function buyerDelete(Request $request)
{
    $buyerEdit = BuyerEdit::findOrFail($request->id);
    $buyerEdit->delete();
    return response()->json(['success' => true]);
}

public function buyerStore(Request $request)
{
    $newRecord = new BuyerEdit;
    $newRecord->title = 'New Title';
    $newRecord->content = 'New Content';
    $newRecord->save();
    return response()->json(['success' => true]);
}

// SELLER ABOUT EDIT PAGE FOR ADMIN

public function sellerUpdate(Request $request)
{
    $sellerEdit = SellerEdit::findOrFail($request->id);
    $sellerEdit->update([
        'title' => $request->title,
        'content' => $request->content
        
    ]);
    return response()->json(['success' => true]);
}

public function sellerDelete(Request $request)
{
    $sellerEdit = SellerEdit::findOrFail($request->id);
    $sellerEdit->delete();
    return redirect()->back();
    return response()->json(['success' => true]);
}
public function sellerStore()
{
$newRecord = new SellerEdit;
$newRecord->title = 'New Title';
$newRecord->content = 'New Content';
$newRecord->save();
return response()->json(['success' => true]);

}
    
//BELOW IS ALL THE PICTURE MODIFICATION FUNCTIONS

//HOME PAGE
public function storePic(Request $request)
{
    $pictures = []; // Create an empty array to store the picture that would be uploaded

    if ($request->hasFile('pictures')) { // Check if there are any uploaded pictures
        foreach ($request->file('pictures') as $picture) { // Loop through each uploaded picture
            $path = $picture->store('public/pictures'); // Store the picture in the 'pictures' directory 
            $pictures[] = Picture::create(['filename' => $path]); // create DB table record for that picture
        }
    }

    return redirect()->back(); // Redirect the user back to the previous page
}
public function destroy($id)
{
    $picture = Picture::findOrFail($id);
    Storage::delete($picture->filename);// delete the picture from the storage directory
     $picture->delete(); //delete from databases
     return redirect()->back();
}

// SAME FUNCTIONALITY BELOW, BUT FOR DIFFERENT PAGES

//ABOUT BUYER
public function buyerPic(Request $request)
{
   
    $pictures = [];
    if ($request->hasFile('pictures')) {
        foreach ($request->file('pictures') as $picture) {
            $path = $picture->store('public/buyer');
            $pictures[] = PicBuyer::create(['filename' => $path]);
        }
    }

    return redirect()->back();
}

public function buyerDestroy($id)
{
    $picture = PicBuyer::findOrFail($id);
    Storage::delete($picture->filename);
    $picture->delete();

    return redirect()->back();
}


//ABOUT SELLER
public function sellerPic(Request $request)
{
    $pictures = [];
    if ($request->hasFile('pictures')) {
        foreach ($request->file('pictures') as $picture) {
            $path = $picture->store('public/seller');
            $pictures[] = PicSeller::create(['filename' => $path]);
        }
    }
    return redirect()->back();
}

public function sellerDestroy($id)
{
    $picture = PicSeller::findOrFail($id);
    Storage::delete($picture->filename);
    $picture->delete();
    return redirect()->back();
}


























}