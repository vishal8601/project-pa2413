<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BidNotification extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'seller_id',
        'buyer_id',
        'project_id',
        'project_name',
        'notification_message'
    ];

    // Relationship to the Project
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    // Relationship to the Seller
    public function seller()
    {
        return $this->belongsTo(Person::class, 'seller_id');
    }

    // Relationship to the Buyer
    public function buyer()
    {
        return $this->belongsTo(Person::class, 'buyer_id');
    }
}