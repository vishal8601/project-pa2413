<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'timelines', 'buyer_id','photos'];

    public function buyer()
    {
        return $this->belongsTo(Person::class, 'buyer_id');
    }
    public function invitations()
{
    return $this->hasMany(Invitation::class);
}

public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    

}


