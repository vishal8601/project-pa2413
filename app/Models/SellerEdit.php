<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellerEdit extends Model
{
    protected $table = 'seller_edits'; // Explicitly specifying the table name associated with the model

    protected $fillable = [
        'title', 'content' // Allowing mass assignment on title and content
    ];

    // You can add more model methods and properties as needed
}
