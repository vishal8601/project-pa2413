<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilePic extends Model
{
    use HasFactory;

    protected $fillable = ['person_id', 'filename'];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }
}
