<?php
// app/Models/Invitation.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'seller_id',
        'buyer_id',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function seller()
    {
        return $this->belongsTo(Person::class, 'seller_id');
    }

    public function buyer()
    {
        return $this->belongsTo(Person::class, 'buyer_id');
    }
}
