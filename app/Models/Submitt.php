<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submitt extends Model
{
    use HasFactory;

    protected $table = 'submitts';  // Specifies the custom table name

    protected $fillable = [
        'project_title',
        'project_description',
        'timelines',
        'buyer_full_name',
        'seller_full_name',
        'bid_amount',
        'bid_comment',
        'buyer_id',
        'seller_email',
        'phone',
        'additional_comment',
      
    ];

    // Relationship to Person (buyer)
    public function buyer()
    {
        return $this->belongsTo(Person::class, 'buyer_id');
    }

    
}
