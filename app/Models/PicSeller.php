<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PicSeller extends Model
{
    use HasFactory;
    protected $table = 'pic_seller';
    protected $fillable = ['filename'];
}
