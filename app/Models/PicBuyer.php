<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PicBuyer extends Model
{
    use HasFactory;


    protected $table = 'pic_buyer';
    protected $fillable = ['filename'];
}
