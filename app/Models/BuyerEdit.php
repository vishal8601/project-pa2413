<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyerEdit extends Model
{
    use HasFactory;

    protected $table = 'buyerEdit'; // Specifying the table name if it's not the plural of the model name

    protected $fillable = [
        'title', 'content'
    ];
}

