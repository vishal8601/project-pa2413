<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Person;
use App\Models\Project;

class Bid extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'seller_id',
        'amount',
        'comment',
    ];

    public function bidder()
    {
        return $this->belongsTo(Person::class, 'seller_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function seller()
{
    return $this->belongsTo(Person::class, 'seller_id'); 

}
}
