<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Enums\AccountStatus;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Person extends Authenticatable
{
    protected $table = 'persons';

    protected $casts = [
        'email_verified_at' => 'datetime',
        // 'status' => AccountStatus::class,
    ];

    protected $fillable = ['first_name', 'last_name', 'email', 'type', 'password', 'verification_token','premium_user'];


    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn($value) => AccountStatus::from($value),
            set: fn(AccountStatus $status) => $status->value,
        );
    }


    // Define the relationship with projects
    public function projects()
    {
        return $this->hasMany(Project::class, 'buyer_id');
    }

    // Define the relationship with services
    public function services()
    {
        return $this->hasMany(Service::class, 'seller_id');
    }

    // Define the relationship with credentials
    public function credentials()
    {
        return $this->hasMany(Credential::class, 'seller_id');
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class, 'seller_id');
    }

    public function bids()
    {
        return $this->hasMany('App\Models\Bid', 'seller_id');
    }

    public function profilePics()
    {
        return $this->hasMany(ProfilePic::class, 'person_id');
    }

    public function blogs()
    {
        return $this->hasMany(Blog::class, 'user_id');
    }




}