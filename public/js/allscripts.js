document.addEventListener('DOMContentLoaded', function() {
    setTimeout(function() {
        var elements = document.querySelectorAll('.status-section');
        elements.forEach(function(element) {
            element.parentNode.removeChild(element);
        });
    }, 5000); // 5000 milliseconds = 5 seconds
});


var menuToggle = document.getElementById('menu-toggle');
    var navbar = document.querySelector('.navbar');

    menuToggle.addEventListener('change', function() {
        if (this.checked) {
            navbar.style.display = 'block';
        } else {
            navbar.style.display = 'none';
        }
    });