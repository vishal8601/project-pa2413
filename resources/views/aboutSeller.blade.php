@extends('layouts.app')

@push('title')
<title>Service Seekers</title>
@endpush

@section('content')

@if (Auth::user() && Auth::user()->status->value == 'blocked')
    <div class="alert alert-danger text-center">
        <h2>Your Account has been suspended</h2>
    </div>
@endif

<div class="landing-page-container">
    <!-- Pictures Section -->
    <div class="pictures-container">
        @foreach($pictures as $picture)
            <div class="picture-wrapper">
                <img src="{{ asset('storage/' . $picture->filename) }}" alt="Seller Image" class="picture">
                @if(auth()->check() && auth()->user()->type === 'admin')
                <form action="{{ route('delete_seller_picture', $picture->id) }}" method="POST" class="delete-form">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
                @endif
            </div>
        @endforeach
    </div>

    <!-- Editable Texts Section -->
    <div class="editable-texts-container">
        @php
        $isVerified = auth()->check() && Auth::user()->type === 'admin';
        @endphp

        @if($isVerified)
        <div class="upload-form-container">
            <form action="{{ route('upload_seller_pictures') }}" method="POST" enctype="multipart/form-data" class="upload-form">
                @csrf
                <input type="file" name="pictures[]" multiple required>
                <button type="submit">Upload Seller Images</button>
            </form>
        </div>
        @endif

        @foreach($sellerEdit as $editableText)
        <div class="editable-record" data-id="{{ $editableText->id }}">
            <h2 class="editableTitle">{{ $editableText->title }}</h2>
            <p class="editableContent">{{ $editableText->content }}</p>

            @if($isVerified)
            <button class="editButton">Edit Text</button>
            <div class="editModal">
                <input type="text" class="editedTitle" value="{{ $editableText->title }}"><br>
                <textarea class="editedContent">{{ $editableText->content }}</textarea>
                <button class="saveButton">Save Changes</button>
                <button class="deleteButton">Delete</button>
                <button class="cancelButton">Cancel</button>
            </div>
            @endif
        </div>
        @endforeach

        @if($isVerified)
        <button id="addRecord">Add Record</button>
        @endif
    </div>
</div>


@endsection

@push('styles')
<style>
.upload-form-container {
    text-align: center;
    margin-bottom: 20px;
}

.upload-form input {
    margin-right: 10px;
}

.landing-page-container {
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    justify-content: center;
    padding: 20px;
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

/* Pictures container */
.pictures-container {
    flex: 1 1 45%;
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    justify-content: center;
}

.picture-wrapper {
    position: relative;
    width: 100%;
    max-width: 300px;
    height: 200px;
    overflow: hidden;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    background-color: #f0f0f0;
}

.picture {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

/* Delete button for images */
.delete-form {
    position: absolute;
    top: 10px;
    right: 10px;
}

.delete-form button {
    background: rgba(255, 255, 255, 0.7);
    border: none;
    border-radius: 5px;
}

/* Editable texts container */
.editable-texts-container {
    flex: 1 1 45%;
    display: flex;
    flex-direction: column;
    gap: 20px;
}

.editable-record {
    padding: 20px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
}

.editable-record h2 {
    font-size: 1.5em;
    margin-bottom: 10px;
}

.editable-record p {
    margin-bottom: 20px;
    line-height: 1.5;
}

.editable-record .editModal {
    display: none;
    margin-top: 10px;
}

.editable-record .editModal input,
.editable-record .editModal textarea {
    width: 100%;
    margin-bottom: 10px;
}

#addRecord {
    margin-top: 20px;
}

.btn {
    transition: background-color 0.3s, transform 0.3s;
}

.btn:hover {
    transform: scale(1.05);
}
</style>
@endpush

@push('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
    $('.editButton').click(function() {
        var $boss = $(this).closest('.editable-record');
        $boss.find('.editedTitle').val($boss.find('.editableTitle').text());
        $boss.find('.editedContent').val($boss.find('.editableContent').text());
        $boss.find('.editModal').show();
    });

    $('.saveButton').click(function() {
        var $boss = $(this).closest('.editable-record');
        var id = $boss.data('id');
        var editedTitle = $boss.find('.editedTitle').val();
        var editedContent = $boss.find('.editedContent').val();
        $.ajax({
            url: "{{ route('seller-edit.update') }}", // Ensure the route is correctly named for seller updates
            type: 'POST',
            data: {
                id: id,
                title: editedTitle,
                content: editedContent,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                if (response.success) {
                    $boss.find('.editableTitle').text(editedTitle);
                    $boss.find('.editableContent').text(editedContent);
                    $boss.find('.editModal').hide();
                }
            }
        });
    });

    $('.deleteButton').click(function() {
        var $boss = $(this).closest('.editable-record');
        var id = $boss.data('id');
        $.ajax({
            url: "{{ route('seller-edit.delete') }}", // Ensure the route is correctly named for seller deletions
            type: 'POST',
            data: {
                id: id,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                if (response.success) {
                    $boss.remove();
                }
            }
        });
    });

    $('.cancelButton').click(function() {
        var $boss = $(this).closest('.editable-record');
        $boss.find('.editModal').hide();
    });

    $('#addRecord').click(function() {
        $.ajax({
            url: "{{ route('seller-edit.store') }}", // Ensure the route is correctly named for seller creations
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                if (response.success) {
                    window.location.reload();
                }
            }
        });
    });
});
</script>

@endpush