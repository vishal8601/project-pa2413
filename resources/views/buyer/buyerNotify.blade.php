@extends('layouts.app')

@push('title')
    <title>Notification</title>
@endpush

@push('styles')
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<style>
    .project-details {
        word-wrap: break-word;
    }
</style>
@endpush

@section('content')
<div class="container mt-5">
    <h1 class="text-success">Closed Projects</h1>
    @if($submitts->isEmpty())
        <p >You have no closed projects yet</p>
    @else

    <!-- where the seller provide everything from contact details, additional comment they like to say.
    and the invoice attachment which was not currently implemented yet due to difficulties -->
        <ul class="list-unstyled">
            @foreach($submitts as $submitt)
                <li class="mb-4 p-3 border rounded border-success bg-light project-details">
                    <strong>Project Title:</strong> {{ $submitt->project_title }}<br>
                    <strong>Description:</strong> {{ $submitt->project_description }}<br>
                    <strong>Timelines:</strong> {{ $submitt->timelines }}<br>
                    <strong>Submitted By:</strong> {{ $submitt->buyer_full_name }}<br>
                    <strong>Seller Name:</strong> {{ $submitt->seller_full_name }}<br>
                    <strong>Bid Amount:</strong> ${{ number_format($submitt->bid_amount, 2) }}<br>
                    <strong>Comment:</strong> {{ $submitt->bid_comment }}<br>
                    <strong>Additional Comment:</strong> {{ $submitt->additional_comment}}<br>
                    <strong>Date Submitted:</strong> {{ $submitt->created_at->format('M d, Y') }}<br>
                    <form action="{{ route('submitts.destroy', $submitt->id) }}" method="POST" class="mt-3">
                        @csrf
                        @method('DELETE') <!-- they have the choice to delete it -->
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </li>
            @endforeach
        </ul>
    @endif
</div>
@endsection

