@extends('layouts.app')


@push('title')
<title>Post Project</title>
@endpush

@section('content')
<div class="main">
    <section class="section">
        <!-- this is for posting the project where by the service seekers, try to make sure the appropiate fields are used -->
        <h2>Project Posting</h2>
        <form action="{{ route('projects.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            
            <label for="project-title">Title:</label>
            <input type="text" id="project-title" name="title" required>

            <label for="project-description">Description:</label>
            <textarea id="project-description" name="description" required></textarea>

            <label for="project-photos">Photos:</label>
            <input type="file" id="project-photos" name="photos[]" multiple>

            <label for="project-timelines">Timelines:</label>
            <input type="date" id="project-timelines" name="timelines" required>

            <input type="submit" value="Post Project">
        </form>
    </section>
</div>
@endsection


@push('styles')
<style>
/* Custom styles for the main content */
.main {
    padding: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
}

.section {
    margin-bottom: 30px;
    width: 100%;
    max-width: 600px;
    border-radius: 10px;
    background-color: #f9f9f9;
    padding: 20px;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
}

.section h2 {
    margin-top: 0;
    margin-bottom: 15px;
    color: #333;
}

/* Custom styles for the form */
form {
    display: flex;
    flex-direction: column;
}

label {
    font-weight: bold;
    margin-bottom: 5px;
}

input[type="text"],
textarea,
input[type="file"] {
    padding: 10px;
    margin-bottom: 15px;
    border: 1px solid #ccc;
    border-radius: 5px;
    width: 100%;
}

input[type="submit"] {
    background-color: #4CAF50;
    color: white;
    border: none;
    padding: 15px 20px;
    font-size: 16px;
    cursor: pointer;
    border-radius: 5px;
    transition: background-color 0.3s ease;
}

input[type="submit"]:hover {
    background-color: #45a049;
}
</style>
@endpush

