<!DOCTYPE html>
<html>
<head>
    <title>Seller Details</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .list-group-item {
            word-wrap: break-word;
            overflow-wrap: break-word;
            border: 1px solid green; /* Green border */
        }
    </style>
</head>
<body class="container mt-5">
    <div class="text-center">
        <h1 class="display-4 text-success"><strong>{{ $person->first_name }} {{ $person->last_name }}</strong></h1>
        <p class="lead">Email: {{ $person->email }}</p>
    </div>

    <h2 class="mt-4 text-success">Services</h2>
    <ul class="list-group">
        @foreach($person->services as $service)
            <li class="list-group-item">
                <strong>{{ $service->name }}</strong><br>{{ $service->description }}
            </li>
        @endforeach
    </ul>

    <h2 class="mt-4 text-success">Credentials</h2>
    <ul class="list-group">
        @foreach($person->credentials as $credential)
            <li class="list-group-item">{{ $credential->credential_name }} from {{ $credential->institution_name }}</li>
        @endforeach
    </ul>

    <h2 class="mt-4 text-success">Experiences</h2>
    <ul class="list-group">
        @foreach($person->experiences as $experience)
            <li class="list-group-item">{{ $experience->job_title }} at {{ $experience->company_name }}<br>
            <strong>Description:</strong>{{ $experience->description}}
        </li>
        @endforeach
    </ul>

    @if($person->profilePics->isNotEmpty())
        <div class="mt-4">
            @foreach($person->profilePics as $profilePic)
                <img src="{{ asset('storage/' . $profilePic->filename) }}" alt="Profile Picture" class="img-fluid mb-2">
            @endforeach
        </div>
    @endif
</body>
</html>


