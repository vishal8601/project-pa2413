@extends('layouts.app')

@push('title')
<title>My Projects</title>
@endpush

@section('content')

<!-- Listing all the projects -->
<section class="project-grid">
    <h2>All Projects</h2>
    <div class="project-container">
        @foreach(auth()->user()->projects as $project)
        <a href="{{route('buyer.each_project',$project->id)}}" class="project-item">
            <h3>{{ $project->title }}</h3>
            @foreach (json_decode($project->photos, true) as $photo)
            <img src="storage/{{ $photo['path'] }}" alt="Project Image" class="post-image">
            @endforeach
            <p>Description: {{ $project->description }}</p>
            <p>Timelines: {{ $project->timelines }}</p>
        </a>
        @endforeach
    </div>
</section>

@endsection

@push('styles')
<style>
.project-grid {
    padding: 20px;
}

.project-container {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
    grid-gap: 20px;
}

.project-item {
    border: 1px solid #ccc;
    padding: 10px;
    background-color: #f9f9f9;
    overflow-wrap: break-word; /* Ensures text wraps within the element */
    word-break: break-word; /* Breaks long words to fit within the container */
}

.project-item h3 {
    margin-top: 0;
    overflow-wrap: break-word; /* Ensures text wraps within the element */
    word-break: break-word; /* Breaks long words to fit within the container */
}

.project-item p {
    overflow-wrap: break-word; /* Ensures text wraps within the element */
    word-break: break-word; /* Breaks long words to fit within the container */
}

.post-image {
    max-width: 100%;
    height: auto;
}
</style>
@endpush
