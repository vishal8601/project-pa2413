@extends('layouts.app')

@push('title')
    <title>{{ $project->title }}</title>
@endpush

@section('content')
<a href="{{ route('buyer.myprojects') }}" class="back-button">&#8592; Back</a> <!-- direct to the page showing all the service provider projects -->
<div class="main">
    <div class="project-details"> <!-- want to show that project choosen in full detail -->
        <h2 class="project-title">{{ $project->title }}</h2>
        <div class="project-photos">
            @foreach (json_decode($project->photos, true) as $photo)
                <img src="{{ asset('storage/' . $photo['path']) }}" alt="Project Image" class="post-image">
            @endforeach
        </div>
        <p class="description"><strong>Description:</strong> {{ $project->description }}</p>
        <p class="timeline"><strong>Timeline:</strong> {{ $project->timelines }}</p>
        <p class="created_at"><strong>Created on:</strong> {{ $project->created_at }}</p>
        <p class="updated_at"><strong>Updated on:</strong> {{ $project->updated_at }}</p>
    </div>

    <div class="bids-section">
        <!-- showing the associated bid for that project where they can accept a bid -->
        <h3>Bids</h3>
        @if($bids->count() > 0)
            <ul>
                @foreach($bids as $bid)
                    <li class="bid-item">
                        <p><strong>Bidder:</strong> {{ $bid->bidder->first_name }} {{ $bid->bidder->last_name }}</p>
                        <p><strong>Amount:</strong> {{ $bid->amount }}</p>
                        <p><strong>Comment:</strong> {{ $bid->comment }}</p>
                        <p><strong>Created at:</strong> {{ $bid->created_at }}</p>
                        @if(!$bid->accepted)
                            <form action="{{ route('accept.bid', $bid->id) }}" method="POST"> <!-- accpet the bid with a button -->
                                @csrf
                                <button type="submit" class="btn btn-success">Accept Bid</button>
                            </form>
                        @else
                            <p class="accepted-bid">Bid accepted!</p>
                        @endif
                    </li>
                    <hr>
                @endforeach
            </ul>
        @else
            <p>No bids yet.</p>
        @endif
    </div>
<!-- where the service seekr or buyer can invite seller through inputting there email of their's,
it will basically send a link to this project -->
    <div class="invitation-section">
        <h3>Invite Seller to Bid</h3>
        <form action="{{ route('invitations.send') }}" method="POST">
            @csrf
            <input type="hidden" name="project_id" value="{{ $project->id }}">
            <label for="seller_email">Enter Seller Email:</label>
            <input type="email" name="seller_email" id="seller_email" required>
            <button type="submit" class="btn btn-primary">Send Invitation</button>
        </form>
    </div>
<!-- Delete project -->
    <div class="delete-project">
        <form method="POST" action="{{ route('buyer.delete_project', ['id' => $project->id]) }}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete Project</button>
        </form>
    </div>

</div>
@endsection

@push('styles')
<style>
    .main {
        padding: 20px;
        max-width: 800px;
        margin: 0 auto;
        background-color: #f9f9f9;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    .project-details {
        padding: 20px;
        background-color: #fff;
        border-radius: 8px;
        margin-bottom: 20px;
    }

    .project-title {
        font-size: 24px;
        margin-bottom: 20px;
    }

    .project-photos {
        display: flex;
        justify-content: center;
        margin-bottom: 20px;
    }

    .post-image {
        max-width: 100%;
        height: auto;
        border-radius: 8px;
        margin: 0 10px;
    }

    .bids-section, .invitation-section, .delete-project {
        background-color: #fff;
        padding: 20px;
        border-radius: 8px;
        margin-bottom: 20px;
    }

    .bid-item {
        padding: 10px 0;
    }

    .accepted-bid {
        color: green;
        font-weight: bold;
    }

    .back-button {
        display: inline-block;
        margin-bottom: 20px;
        padding: 10px 20px;
        background-color: #007bff;
        color: white;
        text-decoration: none;
        border-radius: 8px;
    }

    .back-button:hover {
        background-color: #0056b3;
    }

    .btn {
        display: inline-block;
        margin-top: 10px;
        padding: 10px 20px;
        border-radius: 8px;
        text-decoration: none;
        cursor: pointer;
    }

    .btn-success {
        background-color: #28a745;
        color: white;
    }

    .btn-success:hover {
        background-color: #218838;
    }

    .btn-primary {
        background-color: #007bff;
        color: white;
    }

    .btn-primary:hover {
        background-color: #0056b3;
    }

    .btn-danger {
        background-color: #dc3545;
        color: white;
    }

    .btn-danger:hover {
        background-color: #c82333;
    }

    label {
        display: block;
        margin-top: 10px;
        font-weight: bold;
    }

    input[type="email"], input[type="date"], textarea {
        width: 100%;
        padding: 10px;
        margin-top: 5px;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    hr {
        border: 0;
        border-top: 1px solid #eee;
        margin: 10px 0;
    }
</style>
@endpush
