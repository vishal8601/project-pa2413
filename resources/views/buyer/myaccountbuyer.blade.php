@extends('layouts.app')


@push('title')
<title>My Account</title>
@endpush

@section('content')


<!-- view the buyers account -->
<section class="profile-details">
    <h2>Profile Details</h2>
    <p>First Name: {{ auth()->user()->first_name }}</p>
    <p>Last Name: {{ auth()->user()->last_name }}</p>
    <p>Email: {{ auth()->user()->email }}</p>
    <p>Account Permissions: {{ auth()->user()->type }}</p>
    <!-- pop up to edit their details as an option -->
    <button onclick="openEditPopup()">Edit Details</button>
</section>

<!-- whee they can edit full name only -->
<section class="edit-popup" id="edit-popup">
    <div class="popup-content">
        <span class="close" onclick="closeEditPopup()">&times;</span>
        <h2>Edit Profile</h2>
        <form action="{{ route('buyer.update') }}" method="post">
            @csrf
            
            <label for="edit-first-name">First Name:</label>
            <input type="text" id="edit-first-name" name="first_name" value="{{ auth()->user()->first_name }}"
                required><br>
            @error('first_name')
            <span class="text-danger">{{ $message }}</span><br>
            @enderror

            <label for="edit-last-name">Last Name:</label>
            <input type="text" id="edit-last-name" name="last_name" value="{{ auth()->user()->last_name }}"
                required><br>
            @error('last_name')
            <span class="text-danger">{{ $message }}</span><br>
            @enderror

           

            <button type="submit">Save Changes</button>
        </form>
    </div>
</section>
@endsection



@push('styles')
<style>
h1 {
    margin: 0;
}

section {
    margin: 20px;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 5px;
}

button {
    background-color: #333;
    color: white;
    border: none;
    padding: 5px 10px;
    cursor: pointer;
}

button:hover {
    background-color: #555;
}

.edit-popup {
    display: none;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fefefe;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 5px;
    z-index: 1000;
}

.popup-content {
    text-align: center;
}

.close {
    position: absolute;
    top: 10px;
    right: 10px;
    cursor: pointer;
}

.edit-popup {
    display: none;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fefefe;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 5px;
    z-index: 1000;
}

.popup-content {
    text-align: center;
    margin-bottom: 20px;
}

.popup-content label {
    display: block;
    text-align: left;
    margin-bottom: 5px;
}

.popup-content input {
    width: 100%;
    padding: 8px;
    margin-bottom: 10px;
    box-sizing: border-box;
    border: 1px solid #ccc;
    border-radius: 5px;
}

.popup-content button {
    background-color: #333;
    color: white;
    border: none;
    padding: 10px 15px;
    cursor: pointer;
    border-radius: 5px;
}

.popup-content button:hover {
    background-color: #555;
}
.select-menu {
    appearance: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 8px;
    width: 100%;
    box-sizing: border-box;
    font-size: inherit;
    margin-bottom:3px;
}

.select-menu::after {
    content: '\25BC'; /* down arrow */
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
    pointer-events: none;
}

/* Style for the selected option */
.select-menu option:checked {
    background-color: #f0f0f0;
}
</style>
@endpush


@push('scripts')
<script>
function openEditPopup() {
    document.getElementById('edit-popup').style.display = 'block';
}

function closeEditPopup() {
    document.getElementById('edit-popup').style.display = 'none';
}
</script>
@endpush