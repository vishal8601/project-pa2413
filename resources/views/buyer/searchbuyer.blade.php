@extends('layouts.app')

@push('title')
<title>Search Seller</title>
@endpush


@section('content')
<section class="search-services container mt-5">
    <div class="text-center mb-4">
        <h2>Search Services</h2>
        <form action="{{ route('buyer.search') }}" method="GET" class="form-inline justify-content-center">
            <div class="form-group mx-sm-3 mb-2">
                <input type="text" name="query" id="search-services-input" class="form-control" placeholder="Enter service name...">
            </div>
            <button type="submit" class="btn theButton mb-2">Search</button>
        </form>
    </div>
    <div class="results-container">
        @if ($services->count())
            <ul class="list-group">
            @foreach ($services as $service)
    <li class="list-group-item">
        <a href="{{ route('seller.show', $service->seller_id) }}" class="text-decoration-none">
            <h3 class="h5">{{ $service->name }}</h3>
            <p class="description">{{ $service->description }}</p>
            <p class="text-muted">{{ $service->user->first_name }} {{ $service->user->last_name }}</p>
        </a>
    </li>
@endforeach
            </ul>
            <div class="d-flex justify-content-center mt-4">
                {{ $services->links('pagination::bootstrap-4') }}
            </div>
        @else
            <p class="text-center text-muted">No services found.</p>
        @endif
    </div>
</section>

@push('styles')
<!-- Bootstrap CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<style>
    .search-services h2 {
        color: rgb(0, 162, 0);
    }
    .theButton {
        background-color: rgb(0, 162, 0);
        border-color: rgb(0, 162, 0);
        color: white;
    }
    .theButton:hover {
        background-color: #45a049;
        border-color: #45a049;
    }
    .list-group-item h3 {
        color: #4CAF50;
    }

    .description {
    color: #000;
       }

  .list-group-item:hover .description {
    color: #2e8b57; /* Dark greenish color */
         }
</style>
@endpush
@endsection
