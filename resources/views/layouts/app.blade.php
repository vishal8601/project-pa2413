<!DOCTYPE html>
<html lang="en">

<head>
    @stack('title')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/allstyles.css">

    @stack('styles')


</head>

<body>
    <div class="header">
        <h1>Green Marketplace</h1>
        <p>Make the world green</p>
    </div>

    <input type="checkbox" id="menu-toggle" class="hidden-checkbox">
    <label for="menu-toggle" class="menu-icon">&#9776;</label>

    <div class="navbar">
        <a href="{{ route('home') }}"
            class="nav-link{{ request()->routeIs('home') || request()->is('/') ? ' active' : '' }}">Home</a>






        @if(Auth::check())
            <a href="{{ route('blogposts') }}"
                class="nav-link{{ request()->routeIs('blogposts') ? ' active' : '' }}">Blog</a>
                
            @if(Auth::user()->type == 'seller')
                <a href="{{ route('seller.browse') }}"
                    class="nav-link{{ request()->routeIs('seller.browse') ? ' active' : '' }}">Browse</a>
                <a href="{{ route('seller.myaccount') }}"
                    class="nav-link{{ request()->routeIs('seller.myaccount') ? ' active' : '' }}">My Account</a>
                <a href="{{ route('notify') }}" class="nav-link{{ request()->routeIs('notify') ? ' active' : '' }}">Sell
                    Notification</a>

            @elseif(Auth::user()->type == 'buyer')
                <a href="{{ route('buyer.home') }}" class="nav-link{{ request()->routeIs('buyer.home') ? ' active' : '' }}">Post
                    Project</a>
                <a href="{{ route('buyer.myprojects') }}"
                    class="nav-link{{ request()->routeIs('buyer.myprojects') ? ' active' : '' }}">My Projects</a>
                <a href="{{ route('buyer.search') }}"
                    class="nav-link{{ request()->routeIs('buyer.search') ? ' active' : '' }}">Search Sellers</a>
                <a href="{{ route('buyer.myaccount') }}"
                    class="nav-link{{ request()->routeIs('buyer.myaccount') ? ' active' : '' }}">My Account</a>

                <a href="{{ route('buyerNotify') }}"
                    class="nav-link{{ request()->routeIs('buyerNotify') ? ' active' : '' }}">Notification</a>
            @elseif(Auth::user()->type == 'admin')
                <!-- Admin Links -->
                <a href="{{ route('admin.accounts') }}"
                    class="nav-link{{ request()->routeIs('admin.accounts') ? ' active' : '' }}">Admin Accounts</a>

                <a href="{{ route('aboutBuyer') }}"
                    class="nav-link{{ request()->routeIs('aboutBuyer') ? ' active' : '' }}">About
                    Buyer</a>
                <a href="{{ route('aboutSeller') }}"
                    class="nav-link{{ request()->routeIs('aboutSeller') ? ' active' : '' }}">About
                    Seller</a>





            @endif




            <form action="{{ route('logout') }}" method="POST" class="logout-form">
                @csrf
                <button type="submit" class="nav-link">Log-out</button>
            </form>
        @else
            <a href="{{ route('aboutBuyer') }}"
                class="nav-link{{ request()->routeIs('aboutBuyer') ? ' active' : '' }}">Service Provider</a>
            <a href="{{ route('aboutSeller') }}"
                class="nav-link{{ request()->routeIs('aboutSeller') ? ' active' : '' }}">Service Seeker</a>
            <a href="{{ route('login') }}"
                class="nav-link right{{ request()->routeIs('login') ? ' active' : '' }}">Log-in</a>
        @endif


    </div>


    @if(session('status'))
        <div class="status-section">
            <div class="status-container animate__animated animate__fadeInDown">
                <div class="status">{{ session('status') }}</div>
            </div>
        </div>
    @endif

    @if(session('error'))
        <div class="status-section">
            <div class="status-container animate__animated animate__fadeInDown">
                <div class="error">{{ session('error') }}</div>
            </div>
        </div>
    @endif



    <div style="min-height: 400px;">
        @yield('content')
    </div>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5>Company</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Press Releases</a></li>
                        <li><a href="#">Partnerships</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5>Legal</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Cookie Policy</a></li>
                        <li><a href="#">Copyright Policy</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5>Support</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Feedback</a></li>
                        <li><a href="#">Help Center</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5>Follow Us</h5>
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="#" class="btn btn-social-icon btn-facebook"><i
                                    class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-social-icon btn-twitter"><i
                                    class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-social-icon btn-linkedin"><i
                                    class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-social-icon btn-instagram"><i
                                    class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="/js/allscripts.js"></script>
    @stack('scripts')

</body>

</html>