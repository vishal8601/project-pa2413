@extends('layouts.app')

@push('title')
    <title>Home</title>
@endpush

@section('content')

@if (Auth::user() && Auth::user()->status->value == 'blocked')
    <div class="alert alert-danger text-center">
        <h2>Your Account has been suspended</h2>
    </div>
@endif

<div class="landing-page-container">

<!-- these comment apply to the other landing pages as they are the same -->
    
    <!-- This is where the pictures will be seen the one that is uploaded above -->
    <div class="pictures-container">
        @foreach($pictures as $picture)
            <div class="picture-wrapper">
                <img src="{{ asset('storage/' . $picture->filename) }}" alt="Buyer Image" class="picture">
                @if(auth()->check() && auth()->user()->type === 'admin')
                    <form action="{{ route('delete_picture', $picture->id) }}" method="POST" class="delete-form">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Delete</button>
                    </form>
                @endif
            </div>
        @endforeach
    </div>

    <!-- This section the admin will be able to edit and update the texts for the landing page which is the info for the website -->
    <div class="editable-texts-container">
        @php
        $isVerified = auth()->check() && Auth::user()->type === 'admin';
        @endphp
        <!-- This is where the image will be uploaded -->
        @if($isVerified)
        <div class="upload-form-container">
            <form action="{{ route('upload_pictures') }}" method="POST" enctype="multipart/form-data" class="upload-form">
                @csrf
                <input type="file" name="pictures[]" multiple required>
                <button type="submit">Upload Images</button>
            </form>
        </div>
        @endif
        <!-- this will appear where the admin can modify the text content -->
        @foreach($editableTexts as $editableText)
        <div class="editable-record" data-id="{{ $editableText->id }}">
            <h2 class="editableTitle">{{ $editableText->title }}</h2>
            <p class="editableContent">{{ $editableText->content }}</p>
            @if($isVerified)
            <button class="editButton">Edit Text</button>
            <div class="editModal">
                <input type="text" class="editedTitle" value="{{ $editableText->title }}"><br>
                <textarea class="editedContent">{{ $editableText->content }}</textarea>
                <button class="saveButton">Save Changes</button>
                <button class="deleteButton">Delete</button>
                <button class="cancelButton">Cancel</button>
            </div>
            @endif
        </div>
        @endforeach

        @if($isVerified)
        <button id="addRecord">Add Record</button>
        @endif
    </div>
</div>

@endsection



@push('styles')
<style>
.upload-form-container {
    text-align: center;
    margin-bottom: 20px;
}

.upload-form input {
    margin-right: 10px;
}

.landing-page-container {
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    justify-content: center;
    padding: 20px;
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

.pictures-container {
    flex: 1 1 45%;
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    justify-content: center;
}

.picture-wrapper {
    position: relative;
    width: 100%;
    max-width: 300px;
    height: 200px;
    overflow: hidden;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    background-color: #f0f0f0;
}

.picture {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

.delete-form {
    position: absolute;
    top: 10px;
    right: 10px;
}

.delete-form button {
    background: rgba(255, 255, 255, 0.7);
    border: none;
    border-radius: 5px;
}

.editable-texts-container {
    flex: 1 1 45%;
    display: flex;
    flex-direction: column;
    gap: 20px;
}

.editable-record {
    padding: 20px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
}

.editable-record h2 {
    font-size: 1.5em;
    margin-bottom: 10px;
}

.editable-record p {
    margin-bottom: 20px;
    line-height: 1.5;
}

.editable-record .editModal {
    display: none;
    margin-top: 10px;
}

.editable-record .editModal input,
.editable-record .editModal textarea {
    width: 100%;
    margin-bottom: 10px;
}

#addRecord {
    margin-top: 20px;
}

.btn {
    transition: background-color 0.3s, transform 0.3s;
}

.btn:hover {
    transform: scale(1.05);
}
</style>
@endpush

@push('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
    $('.editButton').click(function() {
        var $boss = $(this).closest('.editable-record'); // Find the closest element with the class 'editable-record'
        // show what is on the sreen into the input fields, so we rewrite and save the changes to associated fields in DB table
        //they are both for different fielss, one title and one Content, this will be consitent throughout this script
        $boss.find('.editedTitle').val($boss.find('.editableTitle').text());
        $boss.find('.editedContent').val($boss.find('.editableContent').text());
        $boss.find('.editModal').show(); // show the edited form
    });

    // this is where the changes are saved
    $('.saveButton').click(function() {
        var $boss = $(this).closest('.editable-record');// Find the closest element with the class 'editable-record'

        //want to get associated ID so that it can be saved in database/server
        var id = $boss.data('id');
        var editedTitle = $boss.find('.editedTitle').val();
        var editedContent = $boss.find('.editedContent').val();
        // save the new data within the database/server
        $.ajax({
            url: "{{ route('editable-text.update') }}",
            type: 'POST',
            data: {
                id: id,
                title: editedTitle,
                content: editedContent,
                _token: '{{ csrf_token() }}'
            },

            // when save, the form will hide and should show the updated field
            success: function(response) {
                if (response.success) {
                    $boss.find('.editableTitle').text(editedTitle);
                    $boss.find('.editableContent').text(editedContent);
                    $boss.find('.editModal').hide();
                }
            }
        });
    });

    /* same concept for delete,
    find closet, get the ID, then when deleted, the field should disappear */

    $('.deleteButton').click(function() {
        var $boss = $(this).closest('.editable-record');
        var id = $boss.data('id');
        $.ajax({
            url: "{{ route('editable-text.delete') }}",
            type: 'POST',
            data: {
                id: id,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                if (response.success) {
                    $boss.remove();
                }
            }
        });
    });
// when press cancel, the form should disappear
    $('.cancelButton').click(function() {
        var $boss = $(this).closest('.editable-record');
        $boss.find('.editModal').hide();
    });

    // to and a new recored
    $('#addRecord').click(function() {
        $.ajax({
            url: "{{ route('editable-text.store') }}",
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                if (response.success) {
                    window.location.reload();
                }
            }
        });
    });
});
</script>
@endpush
