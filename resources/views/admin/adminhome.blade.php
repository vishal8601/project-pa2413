@extends('layouts.app')

@push('title')
<title>Admin Dashboard - Horticulture Website</title>
@endpush

@section('content')
<div class="main">
        <div class="admin-option" onclick="window.location.href='#users';">
            <h3>Manage Users</h3>
            <p>Add, edit, or delete users</p>
        </div>
        <div class="admin-option" onclick="window.location.href='#posts';">
            <h3>Manage Posts</h3>
            <p>View, edit, or delete posts</p>
        </div>
        <div class="admin-option" onclick="window.location.href='#analytics';">
            <h3>Analytics</h3>
            <p>View application analytics</p>
        </div>
    </div>
@endsection






@push('styles')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
<style>

        .main {
            max-width: 960px;
            margin: 40px auto;
            padding: 20px;
            background-color: white;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .admin-option {
            text-align: center;
            padding: 20px;
            margin: 20px;
            background-color: #f0f0f0;
            border-radius: 8px;
            transition: background-color 0.3s ease;
        }

        .admin-option:hover {
            background-color: #e0e0e0;
        }

        .admin-option h3 {
            font-size: 20px;
            color: green;
        }

        .admin-option p {
            font-size: 16px;
            color: #666;
        }

        .footer {
            text-align: center;
            padding: 20px;
            background-color: #343a40;
            color: white;
        }
    </style>
@endpush
