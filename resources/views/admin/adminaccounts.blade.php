@extends('layouts.app')

@push('title')
    <title>Manage Users - Admin Panel</title>
@endpush

@section('content')
<div class="main">
    <table>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Category</th>
            <th>Created Date</th>
            <th>Actions</th>
        </tr>
        @foreach ($persons as $person)
            @if ($person->type === 'seller' || $person->type === 'buyer')
                <tr>
                    <td>{{ $person->first_name }} {{ $person->last_name }}</td>
                    <td><a href="{{ route('user.details', $person->id) }}">{{ $person->email }}</a></td>
                    <td>{{ $person->type }}</td>
                    <td>{{ $person->created_at->toDateString() }}</td>
                    <td class="actions">
                        <button class="delete" data-id="{{ $person->id }}">Delete</button>
                        <button class="suspend" data-id="{{$person->id}}">
                            @if ($person->status->value == 'blocked')
                                Activate
                            @elseif (($person->status->value) == 'active')
                                Suspend
                            @endif
                        </button>
                    </td>
                </tr>
                <form id="delete-form-{{ $person->id }}" action="{{ route('person.delete', $person->id) }}" method="POST"
                    style="display: none;">
                    @csrf
                    @method('DELETE')
                </form>

                @if ($person->status->value == 'blocked')
                    <form id="suspend-form-{{ $person->id }}" action="{{ route('person.un_suspend', $person->id) }}" method="POST"
                        style="display: none;">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="status" value="blocked">
                    </form>
                @else
                    <form id="suspend-form-{{ $person->id }}" action="{{ route('person.suspend', $person->id) }}" method="POST"
                        style="display: none;">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="status" value="blocked">
                    </form>
                @endif


            @endif
        @endforeach
    </table>
</div>
@endsection
@push('styles')
    <style>
        .main {
            width: 90%;
            margin: 20px auto;
            background: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f0f0f0;
        }

        tr:nth-child(even) {
            background-color: #f9f9f9;
        }

        .actions button {
            padding: 5px 10px;
            margin-right: 5px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .footer {
            text-align: center;
            padding: 20px;
            background-color: #343a40;
            color: white;
        }

        .approve {
            background-color: #4CAF50;
            color: white;
        }

        .delete {
            background-color: #f44336;
            color: white;
        }

        .block {
            background-color: #ff9800;
            color: white;
        }

        .suspend {
            background-color: #e91e63;
            color: white;
        }
    </style>
@endpush


@push('scripts')

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            // Select all delete buttons
            var deleteButtons = document.querySelectorAll('.delete');
            var suspendButtons = document.querySelectorAll('.suspend');
            deleteButtons.forEach(function (button) {
                button.addEventListener('click', function () {
                    var personId = this.getAttribute('data-id');
                    if (confirm('Are you sure you want to delete this person?')) {
                        event.preventDefault();
                        document.getElementById('delete-form-' + personId).submit();
                    }
                });
            });
            suspendButtons.forEach(function (button) {
                button.addEventListener('click', function () {
                    var personId = this.getAttribute('data-id');
                    if (confirm('Are you sure you want to change this account status?')) {
                        event.preventDefault();
                        document.getElementById('suspend-form-' + personId).submit();
                    }
                });
            });
        });

    </script>
@endpush

