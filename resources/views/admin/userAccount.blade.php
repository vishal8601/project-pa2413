<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Person Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .container {
            margin-top: 20px;
        }
        .card {
            margin-bottom: 20px;
        }
        .project-image, .profile-image {
            max-width: 400px;
            max-height: 40000px;
            margin-right: 15px;
        }
        ul {
            padding-left: 20px;
        }
        ul li {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
@section('content')
<div class="container">
    <h1 class="mb-4">Person Details</h1>
    <div class="card">
        <div class="card-body">
            <p class="card-text"><strong>Name:</strong> {{ $person->first_name }} {{ $person->last_name }}</p>
            <p class="card-text"><strong>Email:</strong> {{ $person->email }}</p>
            <p class="card-text"><strong>Type:</strong> {{ $person->type }}</p>
            <p class="card-text"><strong>Created On:</strong> {{ $person->created_at->toDateString() }}</p>
    @if($person->type == 'seller')
        <h2 class="mt-4">Services</h2>
        <ul class="list-unstyled">
            @foreach($person->services as $service)
                <li><strong>{{ $service->name }} -</strong>{{ $service->description }}</li>
            @endforeach
        </ul>
        <h2 class="mt-4">Credentials</h2>
        <ul class="list-unstyled">
            @foreach($person->credentials as $credential)
                <li>{{ $credential->credential_name }} from {{ $credential->institution_name }}</li>
            @endforeach
        </ul>
        <h2 class="mt-4">Experiences</h2>
        <ul class="list-unstyled">
            @foreach($person->experiences as $experience)
                <li>{{ $experience->job_title }} at {{ $experience->company_name }}<br>
                <strong>description:</strong>{{$experience->description}}</li>
            @endforeach
        </ul>
        @if($person->profilePics->isNotEmpty())
            <h2 class="mt-4">Profile Pictures</h2>
            <div>
                @foreach($person->profilePics as $profilePic)
                    <img src="{{ asset('storage/' . $profilePic->filename) }}" alt="Profile Picture" class="profile-image">
                @endforeach
            </div>
        @endif
    @elseif($person->type == 'buyer')
        <h2 class="mt-4">Projects</h2>
        <ul class="list-unstyled">
            @foreach($person->projects as $project)
                <li class="mb-3">
                    <strong>Title:</strong> {{ $project->title }}<br>
                    <strong>Description:</strong> {{ $project->description }}<br>
                    <strong>Timelines:</strong> {{ $project->timelines }}<br>
                    @if(!empty($project->photos))
                        <div class="mt-2">
                            @foreach(json_decode($project->photos, true) as $photo)
                                <img src="{{ asset('storage/' . $photo['path']) }}" alt="Project Image" class="project-image">
                            @endforeach
                        </div>
                    @endif
                </li>
            @endforeach
        </ul>
    @endif
    </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
