@extends('layouts.app')

@push('title')
    <title>Checkout Success</title>
@endpush

@section('content')

<h1>Success</h1>
<p>Thank you for subscribing, {{ $person->name }}! You are now a premium user.</p>

@endsection

@push('styles')
    <style>

    </style>
@endpush