@extends('layouts.app')

@push('title')
    <title>Checkout Cancel</title>
@endpush

@section('content')

<h1>Checkout Canceled</h1>
<p>Your subscription process was canceled. Please try again.</p>

@endsection

@push('styles')
    <style>

    </style>
@endpush