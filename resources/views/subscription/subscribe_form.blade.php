<!DOCTYPE html>
<html>
<head>
    <title>Subscribe</title>
    <script src="https://js.stripe.com/v3/"></script>
    <style>
        /* Add some basic styling */
        body {
            font-family: Arial, sans-serif;
            padding: 20px;
            background-color: #f7f7f7;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        form {
            max-width: 400px;
            width: 100%;
            margin: 0 auto;
            padding: 20px;
            background: white;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
            border-radius: 8px;
        }
        .form-row {
            margin-bottom: 20px;
        }
        .form-row label {
            display: block;
            margin-bottom: 8px;
        }
        .form-row input, .form-row button {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .form-row button {
            background-color: #007bff;
            color: white;
            border: none;
            cursor: pointer;
        }
        .form-row button:hover {
            background-color: #0056b3;
        }
        #card-element {
            border: 1px solid #ccc;
            padding: 10px;
            border-radius: 4px;
        }
        #card-errors {
            color: red;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <form id="checkout-form">
        @csrf
        <input type="hidden" name="person_id" value="{{ $person->id }}">

        <div class="form-row">
            <label for="card-element">Credit or Debit Card</label>
            <div id="card-element">
               
            </div>
            <!-- command displaying -->
            <div id="card-errors" role="alert"></div>
        </div>

        <div class="form-row">
            <button type="submit" id="checkout-button">Subscribe</button>
        </div>
    </form>

    <script>
        const stripe = Stripe('{{ env('STRIPE_KEY') }}');
        const elements = stripe.elements();

        // Create an instance of the card Element.
        const card = elements.create('card');

    
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
            const displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        const form = document.getElementById('checkout-form');
        form.addEventListener('submit', async (event) => {
            event.preventDefault();

            const { token, error } = await stripe.createToken(card);
            
            if (error) {
                // If there is an error this will show it.
                const errorElement = document.getElementById('card-errors');
                errorElement.textContent = error.message;
            } else {
                // SThis will send the token to the server.
                const response = await fetch('/create-checkout-session', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    body: JSON.stringify({
                        person_id: '{{ $person->id }}',
                        stripeToken: token.id
                    })
                });

                const session = await response.json();
                
                if (session.error) {
                    // This part will display the error
                    const errorElement = document.getElementById('card-errors');
                    errorElement.textContent = session.error;
                } else {
                    const result = await stripe.redirectToCheckout({ sessionId: session.id });
                    
                    if (result.error) {
                        alert(result.error.message);
                    }
                }
            }
        });
    </script>
</body>
</html>
