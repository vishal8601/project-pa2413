@extends('layouts.app')

@push('title')
<title>Login/Signup</title>
@endpush

@section('content')
<div class="lscontainer">
    <div class="tab">
        <button class="tablinks" onclick="openTing(event, 'Login')">Login</button>
        <button class="tablinks" onclick="openTing(event, 'Signup')">Signup</button>
    </div>
<!-- below is the input fields to login -->
    <div id="Login" class="tabcontent">
        <h2>Login</h2>
        <form action="{{ route('login') }}" method="post">
            @csrf
            <label for="email" class="form-label">Email:</label>
            <input type="email" id="email" name="email" class="input-field" required>
            <label for="password" class="form-label">Password:</label>
            <input type="password" id="password" name="password" class="input-field" required>
            <input type="submit" value="Login" class="submit-btn">
        </form>
    </div>


    <div id="Signup" class="tabcontent">
        <h2>Signup</h2>
        <form action="{{ route('signup.submit') }}" method="post">
            @csrf
            <!-- This is the sign-up form this will be where the user will be able to input the info while regestration
            The text fields can be increased in the future to have phone number, date of birth and such things. -->
            <label for="firstName" class="form-label">First Name:</label>
            <input type="text" id="firstName" name="firstName" class="input-field" required>
    
            <label for="lastName" class="form-label">Last Name:</label>
            <input type="text" id="lastName" name="lastName" class="input-field" required>
    
            <label for="email" class="form-label">Email:</label>
            <input type="email" id="email" name="email" class="input-field" required>
    
            <label for="type" class="form-label">Type:</label>
            <select id="type" name="type" class="input-field" required>
                <option value="buyer">Buyer</option>
                <option value="seller">Seller</option>
            </select>
    
            <input type="password" id="newPassword" name="newPassword" class="input-field" required>
    
            <label for="confirmPassword" class="form-label">Confirm Password:</label>
            <input type="password" id="confirmPassword" name="confirmPassword" class="input-field" required 
                   oninput="check(this)">
    
            <input type="submit" value="Signup" class="submit-btn">
        </form>
    </div>

</div>
@endsection

@push('styles')
<style>
.lscontainer {
    max-width: 400px;
    margin: 0 auto;
    padding: 20px;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

h2 {
    text-align: center;
}

.tab {
    overflow: hidden;
    border-bottom: 1px solid #ccc;
    margin-bottom: 10px;
}

.tab button {
    background-color: inherit;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 10px 15px;
    transition: background-color 0.3s;
}

.tab button:hover {
    background-color: #f0f0f0;
}

.tab button.active {
    background-color: #ccc;
}

.tabcontent {
    display: none;
}

.tabcontent.active {
    display: block;
}

.form-label {
    display: block;
    margin-bottom: 5px;
}

.input-field {
    width: 100%;
    padding: 10px;
    margin: 5px 0;
    border: 1px solid #ccc;
    border-radius: 3px;
    box-sizing: border-box;
}

.submit-btn {
    background-color: #4caf50;
    color: #fff;
    cursor: pointer;
}

.submit-btn:hover {
    background-color: #45a049;
}

.back-btn {
    display: block;
    margin-top: 20px;
    text-align: center;
}

.back-btn a {
    color: #333;
    text-decoration: none;
}
</style>
@endpush


@push('scripts')
<script>

// openTing handles the switching between forms
function openTing(evt, formName) {
    var i, contentTing, linkTing;
    contentTing = document.getElementsByClassName("tabcontent");
    for (i = 0; i < contentTing.length; i++) {
        contentTing[i].style.display = "none";
    }
    linkTing = document.getElementsByClassName("tablinks");
    for (i = 0; i < linkTing.length; i++) {
        linkTing[i].className = linkTing[i].className.replace(" active", "");
    }
    document.getElementById(formName).style.display = "block";
    evt.currentTarget.className += " active";
}
document.getElementsByClassName("tablinks")[0].click(); // Open Login tab by default

function check(input) {
    if (input.value !== document.getElementById('newPassword').value) {
        input.setCustomValidity('Passwords must match.');
    } else {
        input.setCustomValidity('');
    }
}
</script>

@endpush


<!-- W3schools was used to make the log-in and signup form layout-->
