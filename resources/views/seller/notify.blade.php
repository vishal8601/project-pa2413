@extends('layouts.app')

@push('title')
    <title>Seller Notification</title>
@endpush

@push('styles')
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
      p {
       color: black;
      }
    </style>
@endpush

@section('content')
<div class="container mt-4">
    @auth
        
        <h1 class="text-success">Your Accepted Bids</h1>
        <div id="bidsContainer">
            @forelse ($bids as $bid)
                <form id="bidForm-{{ $bid->id }}" method="POST" action="{{ route('submitts.store') }}" class="mb-4"><!-- check mb-4-->
                    @csrf  <!-- CSRF token for security -->
                    <table class="table table-bordered">
                        <tbody>
                        <input type="hidden" name="project_id" value="{{ $bid->project->id }}">
                            <tr>
                                <th>Project Title:</th>
                                <td><input type="text" name="project_title" value="{{ $bid->project->title }}" required readonly class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Project Description:</th>
                                <td><textarea name="project_description" rows="4" readonly class="form-control">{{ $bid->project->description }}</textarea></td>
                            </tr>
                            <tr>
                                <th>Project Timelines:</th>
                                <td><input type="text" name="timelines" value="{{ $bid->project->timelines }}" required readonly class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Buyer's Full Name:</th>
                                <td><input type="text" name="buyer_full_name" value="{{ $bid->project->buyer->first_name }} {{ $bid->project->buyer->last_name }}" required readonly class="form-control"></td>
                            </tr>
                           
                                <td><input type="hidden" name="buyer_id" value="{{ $bid->project->buyer_id }}"></td>
                            
                           
                                <td><input type="hidden"name="seller_full_name" value="{{ $bid->seller->first_name }} {{ $bid->seller->last_name }}" ></td>
                            
                            <tr>
                                <th>Bid Amount:</th>
                                <td><input type="text" name="bid_amount" value="{{ $bid->amount }}" required readonly class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Bid Comment:</th>
                                <td><input type="text" name="bid_comment" value="{{ $bid->comment }}" required readonly class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Seller Email:</th>
                                <td><input type="email" name="seller_email" value="{{ $bid->seller->email }}" required readonly class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Phone Number:</th>
                                <td><input type="text" name="phone"  required class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Additional Comment:</th>
                                <td><input type="text" name="additional_comment"  required class="form-control"></td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            @empty
                <p class="text-muted">You have no accepted bids currently.</p>
            @endforelse
        </div>
        <h1 class="text-success">Your Notifications</h1>
        <div>
            @forelse ($notifications as $notification)
                <div class="alert alert-light border">
                    <p>{{ $notification->notification_message }}</p>
                    <p>Project: {{ $notification->project->title }}</p>
                   
                    @if($notification->project->buyer)
                    @if($notification->notification_message === 'Your bid was accepted')
                        <p><strong>Buyer Name:</strong> {{ $notification->project->buyer->first_name }} {{ $notification->project->buyer->last_name }}</p>
                        <p><strong> Email:</strong> {{ $notification->project->buyer->email }}</p>
                    @endif
                @endif
                    <p><strong>Date:</strong> {{ $notification->created_at->toFormattedDateString() }}</p>
                   <!-- Delete button form -->
                <form action="{{ route('notifications.destroy', $notification->id) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger mt-2">Delete</button>
                </form>
                </div>
            @empty
                <p class="text-muted">No notifications found.</p>
            @endforelse
        </div>
    @else
        <div class="alert alert-info">Welcome, Guest!</div>
        <p>Please <a href="{{ route('login') }}">log in</a> to view your bids.</p>
    @endauth

    <h1 class="text-success">My Invitations</h1>
    @if($invitations->count() > 0)
        <ul class="list-unstyled">
            @foreach($invitations as $invitation)
                <li class="alert alert-light border">
                <h3>Project: {{ $invitation->project->title }}</h3>
                 <p><strong>Buyer:</strong> {{ $invitation->buyer->first_name }} {{ $invitation->buyer->last_name }}</p>
                 <p><strong>Sent on:</strong> {{ $invitation->created_at }}</p>
                 <a href="{{ route('seller.each_project', $invitation->project->id) }}" class="btn btn-success">View Project</a>
                </li>
                <hr>
            @endforeach
        </ul>
    @else
        <p class="text-muted">No invitations found.</p>
    @endif
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('form[id^="bidForm-"]').on('submit', function(e) {
            e.preventDefault(); // Prevent the default form submission
            var form = this;

            $.ajax({
                url: $(form).attr('action'),
                type: 'POST',
                data: $(form).serialize(), // Serialize form data
                success: function(response) {
                    // If the AJAX POST is successful, remove the form
                    $(form).fadeOut(500, function() { $(this).remove(); });
                },
                error: function(xhr, status, error) {
                    // Handle errors here, e.g., show error messages
                    alert('Submission failed: ' + error);
                }
            });
        });
    });
</script>

@endsection
