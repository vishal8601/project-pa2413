@extends('layouts.app')

@push('title')
<title>Browse Posts</title>
@endpush

@section('content')

<div class="bpcontainer">
    <div class="search-bar">
        <form action="{{ route('seller.browse') }}" method="GET">
            <input type="text" name="search" placeholder="Search posts by buyer's name..." class="search-input">
            <button type="submit" class="search-btn">Search</button>
        </form>
    </div>

    <div class="post-feed">
        @forelse ($projects as $project)
        <a class="post" href="{{ route('seller.each_project', $project->id) }}">
            <h2 class="post-title">{{ $project->title }}</h2>
            @foreach (json_decode($project->photos, true) as $index => $photo)
            @if ($index === 0)
            <img src="{{ asset('storage/' . $photo['path']) }}" alt="Project Image" class="post-image">
            @break
            @endif
            @endforeach

            <p class="post-description">{{ $project->description }}</p>
            <div class="post-footer">
                <span class="post-author">Posted by: {{ $project->buyer->first_name }} {{ $project->buyer->last_name }}</span>
                <span class="post-date">Posted on: {{ $project->created_at->format('F d, Y') }}</span>
            </div>
        </a>
        <hr>
        @empty
        <p>No projects found.</p>
        @endforelse
    </div>
</div>

@endsection

@push('styles')
<style>
/* Add your custom styles here */
.bpcontainer {
    max-width: 800px;
    margin: 20px auto;
    padding: 0 20px;
}

.search-bar {
    margin-bottom: 20px;
    display: flex;
    align-items: center;
}

.search-input {
    flex: 1;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
    font-size: 16px;
    margin-right: 10px;
}

.search-btn {
    background-color: #4CAF50;
    color: white;
    border: none;
    border-radius: 5px;
    padding: 10px 20px;
    cursor: pointer;
    transition: background-color 0.3s;
}

.search-btn:hover {
    background-color: #45a049;
}

.post-title {
    font-size: 20px;
    margin-bottom: 10px;
    margin-top: 40px;
    overflow-wrap: break-word; /* Ensures text wraps within the element */
    word-break: break-word; /* Breaks long words to fit within the container */
}

.post-description {
    margin-bottom: 10px;
    overflow-wrap: break-word; /* Ensures text wraps within the element */
    word-break: break-word; /* Breaks long words to fit within the container */
}

.post-image {
    max-width: 60%;
    border-radius: 8px;
    margin-bottom: 10px;
}

.post-footer {
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.post-author {
    font-weight: bold;
}

.post-date {
    color: #999;
}

</style>
@endpush
