@extends('layouts.app')

@push('title')
    <title>{{ $project->title }}</title>
@endpush

@section('content')
<a href="{{ route('seller.browse') }}" class="back-button">&#8592; Back</a>
<div class="main">
    <div class="project-details">
        <h2 class="project-title">{{ $project->title }}</h2>
        <div class="project-photos">
            @foreach (json_decode($project->photos, true) as $photo)
                <img src="{{ asset('storage/' . $photo['path']) }}" alt="Project Image" class="post-image">
            @endforeach
        </div>
        <p class="description"><strong>Description:</strong> {{ $project->description }}</p>
        <p class="timeline"><strong>Timeline:</strong> {{ $project->timelines }}</p>
        <p class="created_at"><strong>Published on:</strong> {{ $project->created_at }}</p>
    </div>

    <div class="bids-comments-section">
        <h3>Bids and Comments</h3>

        <!-- Display Bids -->
        @if($bids->count() > 0)
        <ul class="bids-list">
            @foreach($bids as $bid)
            <li class="bid-item">
                <p><strong>Bidder:</strong> {{ $bid->bidder->first_name }} {{ $bid->bidder->last_name }}</p>
                <p><strong>Amount:</strong> {{ $bid->amount }}</p>
                <p><strong>Comment:</strong> {{ $bid->comment }}</p> <!-- Display comment -->
                <p><strong>Created at:</strong> {{ $bid->created_at }}</p>
            </li>
            <hr>
            @endforeach
        </ul>
        @else
            <p>No bids yet.</p>
        @endif


    </div>

  
    {{--  @if (Auth::user()->premium_user) --}}
        <div class="form-section">
            <h3>Add Bid or Comment:</h3>
            <form action="{{ route('bids.store', ['projectId' => $project->id, 'sellerId' => Auth::user()->id]) }}"
                method="post">
                @csrf
                <label for="amount">Bid Amount:</label>
                <div class="bid-input">
                    <input type="number" name="amount" id="amount" value="10" min="10" step="10" required>
                    <span class="increment">+ $10</span>
                </div>
                <label for="comment">Comment:</label> <!-- Add comment label -->
                <textarea name="comment" id="comment" rows="4" required></textarea> <!-- Textarea for comment -->
                <button type="submit">Submit Bid</button>
            </form>
        </div>
   {{-- @else --}}
        {{-- <h3 style="color:red;">You Must Be Premium User To Add Bids and Comments</h3> --}}
    {{-- @endif --}}

</div>
@endsection

@push('styles')
<style>
.main {
    padding: 20px;
    max-width: 800px;
    margin: 0 auto;
    background-color: #f9f9f9;
    border-radius: 8px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

.back-button {
    display: inline-block;
    margin-bottom: 20px;
    padding: 10px 20px;
    background-color: #007bff;
    color: white;
    text-decoration: none;
    border-radius: 5px;
    font-size: 16px;
}

.back-button:hover {
    background-color: #0056b3;
}

.project-details {
    padding: 20px;
    background-color: #fff;
    border-radius: 8px;
    margin-bottom: 20px;
    text-align: left;
}

.project-title {
    font-size: 24px;
    margin-bottom: 20px;
}

.project-photos {
    display: flex;
    justify-content: center;
    margin-bottom: 20px;
}

.post-image {
    max-width: 100%;
    height: auto;
    border-radius: 8px;
    margin: 0 10px;
}

.bids-comments-section, .form-section {
    padding: 20px;
    background-color: #fff;
    border-radius: 8px;
    margin-bottom: 20px;
    text-align: left;
}

.bids-list {
    list-style-type: none;
    padding: 0;
}

.bid-item {
    padding: 10px 0;
}

hr {
    border: 0;
    border-top: 1px solid #eee;
    margin: 10px 0;
}

        .bid-input {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }

.bid-input input {
    flex: 1;
    margin-right: 10px;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
}

textarea {
    width: 100%;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    margin-bottom: 10px;
}

.btn-submit {
    display: inline-block;
    padding: 10px 20px;
    background-color: #28a745;
    color: white;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;
}

.btn-submit:hover {
    background-color: #218838;
}
</style>
@endpush