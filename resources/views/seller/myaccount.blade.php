@extends('layouts.app')

@push('title')
<title>My Account</title>
@endpush

@section('content')

<div class="main">


<div class="Person">
    <!-- To show the seller first name and their email-->
    <p>
        @auth
        {{ Auth::user()->first_name }}
        @else
        Guest
        @endauth
    </p>
    <p>
        @auth
        {{ Auth::user()->email }}
        @else
        Guest
        @endauth
    </p>
</div>

@if (Auth::user()->premium_user)
    <h2 style="color:gold;">Premium User</h2>
@else
    <div class="premium-link">
        <a href="{{ route('subscribe.form', ['personId' => Auth::id()]) }}">Upgrade to Premium</a>
    </div>
@endif

<!-- Retreieve the seller's profile information based on related user thats is logged in.
     Others cannot see other's details for anything as it must only show the the details based on the seller that is logged in.
-->

<!-- PROVIDE COMMENT FOR SERVICE LISTING ONLY, AS CREDENTIALS AND EXPERIENCES ARRE THE SAME CODE AND FUNCTIONALITY -->
    
<div class="theTables">
<table >
     <thead>
       <tr><th>Services</th></tr>
     </thead>
     <tbody>
     @isset($services)
        @forelse ($services as $service) <!-- retrieve all services, so each service that is linked to that seller -->
        <tr>
          
   <td>
  <!-- We retieve the service listing-->
      <strong>{{ $service->name }}</strong><br>
      <p>{{ $service->description }}</p>

  <!-- the pop-up is where the seller can modify or delete that accosiated service.
       Seller press edit to show associated servie where they can do that -->         
     <button onclick="openEditPopup('editForm{{ $service->id }}')">Edit</button> <!-- retrieve the associsated service by for modification when within pop-up -->
  <!-- We using javacript, which code is below for more explanation -->
      <div id="editForm{{ $service->id }}" class="edit-popup">
  <!-- Below is the function to update the service, inputs to change the text and button to update the changes made -->
     <form action="{{ route('update-service', $service->id) }}" method="POST"> <!-- We retrieve the function that updates service that is defined in web.php-->
  <!-- Post is a method that tells this form to send the new data to the server or database -->
     @csrf <!-- For protection, basically make sure that the seller logged in can do this task, no outside this can -->
     @method('PUT')<!-- simulate updadting the existing data-->
  <!-- below is the input fields for updating -->
     <label for="name">Name:</label>
     <input type="text" name="name" value="{{ $service->name }}" required>
     <label for="description">Description:</label>
     <textarea name="description" required>{{ $service->description }}</textarea>
     <button type="submit">Update</button>
   <!-- for option to cancel or close the pop up -->
     <button type="button" onclick="closeEditPopup('editForm{{ $service->id }}')">Close</button>
     </form>
    <!-- this also popup where seller can delete the associated service -->
    <form action="{{ route('delete-service', $service->id) }}" method="POST"> <!-- retrieve the function for deletion -->
     @csrf
     @method('DELETE') <!-- simulate a DELETE request -->
    <!-- A pop to make sure user want to delete, a two step process for deletion -->
    <button type="submit" onclick="return confirm('Are you sure you want to delete this service?')">Delete</button> 
    </form>
    </div>
    </td>
    </tr>

    <!-- below shows that if empty, display that text stating that-->
    @empty
      <tr>
        <td>No services available.</td>
            </tr>
            @endforelse
            @endisset
        </tbody>
    </table>
    <!-- This for adding a new service that will be linked to the seller -->
    <form method="POST" action="{{ route('services.store') }}"> <!-- use the function for adding new service -->
        @csrf
        <div>
            <label for="name">Service Name:</label>
            <input type="text" id="name" name="name" class="add" required>
        </div>
        <div>
            <label for="description">Description:</label>
            <textarea id="description" name="description" class="add" required></textarea>
        </div>
        <button type="submit">Add Service</button>
    </form>


    <!-- CREDENTIALS-->
    <table>
        <thead>
            <tr>
                <th>Credentials</th>
            </tr>
        </thead>
        <tbody>
            @isset($credentials)
            @forelse ($credentials as $credential)
            <tr>
                <td>
                    <strong>{{ $credential->credential_name }}</strong><br>
                    <p>{{ $credential->institution_name }}</p>
                    <button onclick="openEditPopup('editCredentialForm{{ $credential->id }}')">Edit</button>
                    <div id="editCredentialForm{{ $credential->id }}" class="edit-popup">
                        <form action="{{ route('update-credential', $credential->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <label for="credential_name">Credential Name:</label>
                            <input type="text" name="credential_name" value="{{ $credential->credential_name }}" required>
                            <label for="institution_name">Institution Name:</label>
                            <input type="text" name="institution_name" value="{{ $credential->institution_name }}" required>
                            <button type="submit">Update</button>
                            <button type="button" onclick="closeEditPopup('editCredentialForm{{ $credential->id }}')">Close</button>
                        </form>
                        <form action="{{ route('delete-credential', $credential->id) }}" method="POST" style="display:inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" onclick="return confirm('Are you sure you want to delete this credential?')">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr>
                <td>No credentials available.</td>
            </tr>
            @endforelse
            @else
            <tr>
                <td>No credentials available.</td>
            @endisset
        </tbody>
    </table>

    <!-- ADDING NEW CREDENTIALS -->
    <form method="POST" action="{{ route('credentials.store') }}">
        @csrf
        <div>
            <label for="credential_name">Credential Name:</label>
            <input type="text" id="credential_name" name="credential_name" class="add" required>
        </div>
        <div>
            <label for="institution_name">Institution Name:</label>
            <input type="text" id="institution_name" name="institution_name" class="add" required>
        </div>
        <button type="submit">Add Credential</button>
    </form>

    <!-- EXPRIENCES -->
    <table>
        <thead>
            <tr>
                <th>Experience</th>
            </tr>
        </thead>
        <tbody>
            @isset($experiences)
            @forelse ($experiences as $experience)
            <tr>
                <td >
                    <strong>{{ $experience->job_title }}</strong><br>
                    <p>{{ $experience->company_name }}</p>
                    <p>{{ $experience->description }}</p>
                    <button onclick="openEditPopup('editExperienceForm{{ $experience->id }}')">Edit</button>
                    <div id="editExperienceForm{{ $experience->id }}" class="edit-popup">
                        <form action="{{ route('update-experience', $experience->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <label for="job_title">Job Title:</label>
                            <input type="text" name="job_title" value="{{ $experience->job_title }}" required>
                            <label for="company_name">Company Name:</label>
                            <input type="text" name="company_name" value="{{ $experience->company_name }}" required>
                            <label for="description">Description:</label>
                            <textarea name="description" required>{{ $experience->description }}</textarea>
                            <button type="submit">Update</button>
                            <button type="button" onclick="closeEditPopup('editExperienceForm{{ $experience->id }}')">Close</button>
                        </form>
                        <form action="{{ route('delete-experience', $experience->id) }}" method="POST" style="display:inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" onclick="return confirm('Are you sure you want to delete this experience?')">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr>
                <td>No experiences available.</td>
            </tr>
            @endforelse
            @endisset
        </tbody>
    </table>

    <!-- Add experience form -->
    <div class="form-container">
        <h3>Add Work Experience</h3>
        <form method="POST" action="{{ route('store.experience') }}">
            @csrf
            <div class="form-group">
                <label for="job_title">Job Title:</label>
                <input type="text"   id="job_title" name="job_title" class="add" required>
            </div>
            <div class="form-group">
                <label for="company_name">Company Name:</label>
                <input type="text"  id="company_name"  class="add" name="company_name">
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea  id="description" name="description" class="add" ></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Add Experience</button>
        </form>
    </div>

</div>


<!-- below is where the user can add and delete photos, there are limited to only three photos -->
    <div class="thePics">
    <h1>MyPhotos</h1>
    @if($pictures->count() < 3) <!-- Limit to three -->
        <form action="{{ route('profile_pictures') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="pictures[]" multiple required> <!-- picture saved as an array -->
            <button type="submit">Upload Images</button>
        </form>
        <!-- The form disappears when there is three pictures -->
    @else
        <p>You have already uploaded 3 pictures.</p> 
    @endif
 
    <!-- below is to delete the associated picture -->
    @foreach($pictures as $picture)
    
<!-- Display the user's picture from the storage directory,-->
<!-- if cannot show picture, shows the alt text, 'User Picture' -->
        <img src="{{ asset('storage/' . $picture->filename) }}" alt="User Picture">
        <form action="{{ route('delete_profile_pic', $picture->id) }}" method="POST"> <!-- we get the function -->
            @csrf
            @method('DELETE')
            <button type="submit">Delete</button>
        </form>
    @endforeach
</div>
</div>

@endsection

@push('styles')
<style>
    .premium-link a{
        border: 4px solid red;
        margin-bottom: 10px;
    }
   .premium-link a {
        display: inline-block;
        padding: 10px 20px;
        background-color: #007bff;
        color: white;
        text-decoration: none;
        border-radius: 5px;
        transition: background-color 0.3s;
    }

    .premium-link a:hover {
        background-color: #0056b3;
    }


input[type="text"],
textarea,
select {
    padding: 10px;
    margin-bottom: 10px;
    width: 100%;
    border: 1px solid #ccc;
    border-radius: 5px;
    box-sizing: border-box;
    font-size: 16px;
}

label {
    display: block;
    font-size: 20px;
    font-weight: bold;
    margin-bottom: 5px;
}

button {
    padding: 10px 20px;
    background-color: #007bff;
    color: white;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    font-size: 16px;
    transition: background-color 0.3s;
    display: inline-block;
    margin: 10px 0;
}

button:hover {
    background-color: #0056b3;
}

.edit-popup {
    display: none;
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0, 0, 0, 0.4);
}

.edit-popup form {
    background-color: white;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 50%;
}

table {
    border-collapse: collapse;
    width: 100%; /* Adjusted to fit the container */
    border-radius: 80px;
    table-layout: fixed;
    font-family: Arial, sans-serif;
}

th, td {
    border: 2px solid green;
    padding: 10px;
    overflow: hidden;
    word-wrap: break-word;
    text-overflow: ellipsis;
}

th {
    text-align: center;
}

td:hover {
    background-color: rgba(0, 128, 0, 0.1);
}

input.add, textarea.add {
    overflow: hidden;
    word-wrap: break-word;
    text-overflow: ellipsis;
    width: 100%;
}

.main-container {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    gap: 20px;
}

.theTables {
    width: 45%;
}

.thePics {
    width: 55%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
}

.thePics img {
    width: 100%;
    margin-bottom: 20px;
}

.Person {
    font-size: 30px;

}




</style>
@endpush

@push('scripts')
<script>
// Javascript to open and close pop-up

//Open pop up
function openEditPopup(formId) {
    document.getElementById(formId).style.display = 'block';
}


//Close pop-up
function closeEditPopup(formId) {
    document.getElementById(formId).style.display = 'none';
}
</script>
@endpush
