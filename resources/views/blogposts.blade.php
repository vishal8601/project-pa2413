@extends('layouts.app')

@push('title')
    <title>Blog Post</title>
@endpush

@section('content')
<div class="container">


<!-- to show all the blogs avaliable -->
    @foreach($blogs as $blog)
        <div class="blog" id="blog_{{ $blog->id }}">
            <div id="content_{{ $blog->id }}">
                <h2 id="blog_title_{{ $blog->id }}">{{ $blog->title }}</h2>
                <p id="blog_content_{{ $blog->id }}">{{ $blog->content }}</p>
                <p class="blogauthor">{{ $blog->author->first_name . ' ' . $blog->author->last_name }}</p>
                @if($blog->image)
                    <img id="blog_image_{{$blog->id}}" src="{{ asset('storage/public/images/' . $blog->image) }}"
                        alt="Blog Image">
                @endif

                <!-- This buttons will be available to the user that hgas posted it. It will dtect who was the user that posted it. -->
                @if (Auth::check() && Auth::user()->id == $blog->author->id)
                    <div class="actions">
                        <button class="edit-button" onclick="toggleEditForm({{ $blog->id}})">Edit</button>
                        <button class="delete-button" onclick="submitDeleteForm({{ $blog->id}})">Delete</button>
                    </div>
                @elseif (Auth::check() && Auth::user()->type == 'admin')
                    <div class="actions">
                        <button class="delete-button" onclick="submitDeleteForm({{ $blog->id }})">Delete</button>
                    </div>
                @endif
            </div>

            <!-- Edit Form -->
            <form id="edit_form_{{ $blog->id }}" action="{{ route('blogs.update', $blog->id) }}" method="POST"
                style="display: none;" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="text" name="title" id="edit_title_{{ $blog->id }}" value="{{ $blog->title }}">
                <textarea name="content" id="edit_content_{{ $blog->id }}">{{ $blog->content }}</textarea>
                <input type="file" name="image" id="edit_image_{{ $blog->id }}">
                @if($blog->image)
                    <img src="{{ asset('storage/public/images/' . $blog->image) }}" alt="Blog Image">
                @endif
                <button type="submit">Save</button>
            </form>

            <!-- Delete Form -->
            <form id="delete_form_{{ $blog->id }}" action="{{ route('blogs.destroy', $blog->id) }}" method="POST"
                style="display: none;">
                @csrf
                @method('DELETE')
            </form>
        </div>
    @endforeach





    <!-- Button to Open the Modal -->
    <label for="modal-toggle" class="modal-btn">Add Blog</label>
    <input type="checkbox" id="modal-toggle">

    <!-- The Modal -->
    <!-- This is the popup to add blog -->
    <div class="custom-modal">
        <div class="custom-modal-content">
            <span class="custom-close" onclick="document.getElementById('modal-toggle').checked = false;">&times;</span>
            <form method="POST" action="{{ route('blogs.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" required>
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" id="content" name="content" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                </div>
                <button type="submit" class="modal-btn">Submit</button>
            </form>
        </div>
    </div>

</div>

@endsection

@push('styles')
    <style>
        .container {
            width: 90%;
            margin: 0 auto;
            padding: 20px 0;
        }

        /* Blog Card Styles */
        .blog {
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
            overflow: hidden;
            transition: transform 0.3s, box-shadow 0.3s;
            cursor: pointer;
            padding: 20px;
        }

        .blog:hover {
            transform: translateY(-10px);
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
        }

        .blog h2 {
            margin-top: 0;
            font-size: 24px;
            color: #333;
        }

        .blog p {
            font-size: 16px;
            color: #666;
        }

        .blog img {
            max-width: 100%;
            max-height: 300px;
            width: auto;
            height: auto;
            border-radius: 10px;
            margin-top: 10px;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .blog form {
            margin-top: 20px;
        }

        .blog input[type="text"],
        .blog textarea,
        .blog input[type="file"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
            font-size: 16px;
        }

        .blog input[type="text"]:focus,
        .blog textarea:focus,
        .blog input[type="file"]:focus {
            border-color: #007bff;
            box-shadow: 0 0 5px rgba(0, 123, 255, 0.5);
            outline: none;
        }

        .blog textarea {
            height: 150px;
        }

        .blog button {
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }

        .blog button:hover {
            background-color: #0056b3;
        }

        .modal-btn {
            display: inline-block;
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 5px;
            margin: 20px;
            transition: background-color 0.3s;
        }

        .modal-btn:hover {
            background-color: #0056b3;
        }

        /* Hidden checkbox to control modal visibility */
        #modal-toggle {
            display: none;
        }

        /* Modal styles */
        .custom-modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
        }

        /* Modal content */
        .custom-modal-content {
            background-color: #fefefe;
            margin: 10% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 90%;
            max-width: 500px;
            border-radius: 10px;
            position: relative;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
            animation: fadeIn 0.3s;
        }

        /* Keyframes for fadeIn animation */
        @keyframes fadeIn {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        /* Close button */
        .custom-close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
            cursor: pointer;
        }

        .custom-close:hover,
        .custom-close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        /* Show modal when checkbox is checked */
        #modal-toggle:checked+.custom-modal {
            display: block;
        }

        /* Form styling */
        .form-group {
            margin-bottom: 15px;
        }

        .form-group label {
            display: block;
            margin-bottom: 5px;
            font-weight: bold;
        }

        .form-control,
        .form-control-file {
            width: 100%;
            padding: 10px;
            font-size: 16px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
        }

        .form-control:focus,
        .form-control-file:focus {
            border-color: #007bff;
            box-shadow: 0 0 5px rgba(0, 123, 255, 0.5);
            outline: none;
        }

        .modal-btn {
            display: inline-block;
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 5px;
            margin-top: 10px;
            transition: background-color 0.3s;
        }

        .modal-btn:hover {
            background-color: #0056b3;
        }
        .delete-button{
            background-color: red;
        }
    </style>
@endpush

@push('scripts')
    <script>
        // switich from view to the view to edit the foem
        function toggleEditForm(blogId) {
            const contentDiv = document.getElementById('content_' + blogId);
            const editForm = document.getElementById('edit_form_' + blogId);

            if (editForm.style.display === 'none') {
                contentDiv.style.display = 'none';
                editForm.style.display = 'block';
            } else {
                contentDiv.style.display = 'block';
                editForm.style.display = 'none';
            }
        }
// also delete button when it pops up 
        function submitDeleteForm(blogId) {
            document.getElementById('delete_form_' + blogId).submit();
        }
    </script>
@endpush
