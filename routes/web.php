<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BuyerController;
use App\Http\Controllers\SellerController;
use App\Http\Middleware\CheckAcccountStatus;

use App\Http\Controllers\EditableTextController;

use App\Http\Controllers\EmailController;

use App\Http\Controllers\BlogController;

use App\Http\Controllers\BidController;

use App\Http\Controllers\SignupController;

use App\Http\Controllers\LoginController;

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ImageController;

use App\Http\Controllers\PremiumController;


//Signing Up

Route::get('/signup', [SignupController::class, 'showForm'])->name('signup.form');
Route::post('/signup', [SignupController::class, 'store'])->name('signup.submit');


Route::get('/verify-email', [EmailController::class, 'verifyEmail'])->name('verify.email');

//Logging in

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/aboutBuyer', [HomeController::class, 'aboutBuyer'])->name('aboutBuyer');
Route::get('/aboutSeller', [HomeController::class, 'aboutSeller'])->name('aboutSeller');



Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/home', [EditableTextController::class, 'index'])->name('home');


// Edit the homepage
Route::get('/', [EditableTextController::class, 'index']);

Route::middleware(['auth', CheckAcccountStatus::class])->group(function () {
    //Navigation

    // use App\Models\EditableText;



    Route::post('/editable-text/update', [EditableTextController::class, 'update'])->name('editable-text.update');
    Route::post('/editable-text/delete', [EditableTextController::class, 'delete'])->name('editable-text.delete');
    Route::post('/editable-text/store', [EditableTextController::class, 'store'])->name('editable-text.store');



    // stripe integration
    Route::get('/subscribe/{personId}', [PremiumController::class, 'showSubscribeForm'])->name('subscribe.form');
    Route::post('/create-checkout-session', [PremiumController::class, 'createCheckoutSession']);
    Route::get('/checkout/success', [PremiumController::class, 'success'])->name('checkout.success');
    Route::get('/checkout/cancel', [PremiumController::class, 'cancel'])->name('checkout.cancel');



   



    Route::get('/blogs', [BlogController::class, 'index'])->name('blogposts');
    Route::post('/blogs/store', [BlogController::class, 'store'])->name('blogs.store');
    Route::put('/blogs/update/{blog}', [BlogController::class, 'update'])->name('blogs.update');
    Route::delete('/blogs/{blog}', [BlogController::class, 'destroy'])->name('blogs.destroy');


    Route::get('/adminaccounts', [AdminController::class, 'adminaccounts'])->name('admin.accounts');
    Route::get('/adminhome', [AdminController::class, 'adminhome'])->name('admin.home');
    Route::get('/adminmyaccount', [AdminController::class, 'adminmyaccount'])->name('admin.myaccount');
    Route::get('/adminposts', [AdminController::class, 'adminposts'])->name('admin.posts');
    Route::delete('/delete-person/{id}', [AdminController::class, 'deletePerson'])->name('person.delete');
    Route::post('/suspend-person/{id}', [AdminController::class, 'suspendPerson'])->name('person.suspend');
    Route::post('/un-suspend-person/{id}', [AdminController::class, 'unSuspendPerson'])->name('person.un_suspend');
    Route::get('/user/{id}', [AdminController::class, 'showPersonDetails'])->name('user.details');


    Route::get('/buyer', [BuyerController::class, 'buyer'])->name('buyer.home');
    Route::get('/myaccountbuyer', [BuyerController::class, 'myaccountbuyer'])->name('buyer.myaccount');
    Route::get('/myprojects', [BuyerController::class, 'myprojects'])->name('buyer.myprojects');
    Route::get('/project/{id}', [BuyerController::class, 'each_project'])->name('buyer.each_project');
    Route::get('/searchbuyer', [BuyerController::class, 'searching'])->name('buyer.search');
    Route::get('/person/{id}', [BuyerController::class, 'show'])->name('seller.show');
    Route::post('/invitations/send', [BuyerController::class, 'send'])->name('invitations.send');
    Route::delete('project/delete/{id}', [BuyerController::class, 'delete_project'])->name('buyer.delete_project');



    Route::get('/myaccount', [SellerController::class, 'myaccount'])->name('seller.myaccount');
    Route::get('/seller', [SellerController::class, 'seller'])->name('seller.home');
    Route::get('/sellerbrowse', [SellerController::class, 'sellerbrowse'])->name('seller.browse');
    Route::get('/seller/project/{id}', [SellerController::class, 'each_project'])->name('seller.each_project');



    Route::post('/accept-bid/{bid}', [BidController::class, 'acceptBid'])->name('accept.bid');
    Route::get('/notify', [BidController::class, 'notify'])->name('notify');
    Route::get('/buyerNotify', [BidController::class, 'buyerNotify'])->name('buyerNotify');
    Route::post('/submitts/store', [BidController::class, 'storeSubmitt'])->name('submitts.store');
    Route::delete('/submitts/{id}', [BidController::class, 'destroySubmitt'])->name('submitts.destroy');
    Route::delete('/notifications/{id}', [BidController::class, 'destroyNotify'])->name('notifications.destroy');



    // for bids and comments
    Route::post('/projects/{projectId}/{sellerId}/bids', [SellerController::class, 'bid_store'])->name('bids.store');







    Route::post('/buyer-edit/update', [EditableTextController::class, 'buyerUpdate'])->name('buyer-edit.update');
    Route::post('/buyer-edit/delete', [EditableTextController::class, 'buyerDelete'])->name('buyer-edit.delete');
    Route::post('/buyer-edit/store', [EditableTextController::class, 'buyerStore'])->name('buyer-edit.store');

    Route::post('/seller-edit/update', [EditableTextController::class, 'sellerupdate'])->name('seller-edit.update');
    Route::post('/seller-edit/delete', [EditableTextController::class, 'sellerdelete'])->name('seller-edit.delete');
    Route::post('/seller-edit/store', [EditableTextController::class, 'sellerstore'])->name('seller-edit.store');

    Route::post('/upload_pictures', [EditableTextController::class, 'storePic'])->name('upload_pictures');
    Route::delete('/delete_picture/{id}', [EditableTextController::class, 'destroy'])->name('delete_picture');


    Route::post('/buyer_upload_pictures', [EditableTextController::class, 'buyerPic'])->name('buyer_upload_pictures');
    Route::delete('/buyer_delete_picture/{id}', [EditableTextController::class, 'buyerDestroy'])->name('buyer_delete_picture');


    Route::post('/upload_seller_pictures', [EditableTextController::class, 'sellerPic'])->name('upload_seller_pictures');
    Route::delete('/delete_seller_picture/{id}', [EditableTextController::class, 'sellerDestroy'])->name('delete_seller_picture');




    Route::post('/projects', [ProjectController::class, 'store'])->name('projects.store');


    Route::post('/buyer/update', [BuyerController::class, 'update'])->name('buyer.update');




    // Displaying services and credentials
    Route::middleware(['auth'])->group(function () {
        Route::get('/seller/myaccount', [ProfileController::class, 'myAccount'])->name('seller.myaccount');
    });
    // Storing services
    Route::post('/seller/myaccount/services', [ProfileController::class, 'store'])->name('services.store');
    // Storing credentials
    Route::post('/seller/myaccount/credentials', [ProfileController::class, 'CredStore'])->name('credentials.store');



    Route::put('/update-service/{id}', [ProfileController::class, 'updateService'])->name('update-service');
    Route::delete('/delete-service/{id}', [ProfileController::class, 'deleteService'])->name('delete-service');


    Route::put('/update-credential/{id}', [ProfileController::class, 'updateCredential'])->name('update-credential');
    Route::delete('/delete-credential/{id}', [ProfileController::class, 'deleteCredential'])->name('delete-credential');



    Route::post('/store-experience', [ProfileController::class, 'storeExperience'])->name('store.experience');

    Route::put('/update-experience/{id}', [ProfileController::class, 'updateExperience'])->name('update-experience');
    Route::delete('/delete-experience/{id}', [ProfileController::class, 'deleteExperience'])->name('delete-experience');

    Route::post('/profile/photos', [ProfileController::class, 'profilePic'])->name('profile_pictures');
    Route::delete('/profile/photos/{id}', [ProfileController::class, 'profileDestroy'])->name('delete_profile_pic');




});


