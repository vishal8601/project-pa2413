<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bid_notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('seller_id');  // ID of the seller receiving the notification
            $table->unsignedBigInteger('buyer_id');   // ID of the buyer who posted the project
            $table->unsignedBigInteger('project_id'); // ID of the project related to the bid
            $table->string('project_name');           // Name of the project
            $table->string('notification_message');   // Message indicating if the bid was accepted or rejected
            $table->timestamps();

            $table->foreign('seller_id')->references('id')->on('persons')->onDelete('cascade');
            $table->foreign('buyer_id')->references('id')->on('persons')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bid_notifications');
    }
};
