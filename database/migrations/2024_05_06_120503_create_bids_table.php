<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('seller_id');
            $table->text('comment');
            $table->decimal('amount', 8, 2);
            $table->boolean('accepted')->default(false);
            $table->boolean('rejected')->default(false); // New column to mark bids as rejected
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('seller_id')
                ->references('id')
                ->on('persons')  // Assuming 'persons' table has a 'type' field to distinguish sellers
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bids');
    }
};




