<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('submitts', function (Blueprint $table) {
            $table->id();
            $table->string('project_title')->nullable();
            $table->text('project_description')->nullable();
            $table->text('timelines')->nullable();
            $table->string('buyer_full_name')->nullable();
            $table->string('seller_full_name')->nullable();
            $table->decimal('bid_amount', 10, 2)->nullable();
            $table->text('bid_comment')->nullable();
            $table->unsignedBigInteger('buyer_id')->nullable();  // Add the buyer_id column
            $table->string('seller_email')->nullable();
            $table->string('phone')->nullable();
            $table->string('additional_comment');
            $table->timestamps();
            $table->foreign('buyer_id')->references('id')->on('persons')->onDelete('set null');  // Adding the foreign key constraint
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('submitts');
    }
};



