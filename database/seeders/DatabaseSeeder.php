<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder ;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        DB::table('persons')->insert([
            'first_name' => 'HorticultureAdmin',
            'last_name' => 'Admin',
            'email' => 'admin@hwb.com',
            'verification_token' => Str::random(64),
            'password' => Hash::make('password'),
            'created_at' => now(),
            'updated_at' => now(),
            'email_verified_at' => now(),
            'type' => 'admin',
        ]);
    }
}
